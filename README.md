# Install education-portal-server

```shell
cat kubernetes/education-portal-server.yaml |
  sed "s|{{DB_HOST}}|a973f6cf-fce5-4920-8d47-50813320bc8c.priv.instances.scw.cloud|;
    s|{{DB_NAME}}|edu-server|;
    s|{{DB_USER}}|edu-server|;
    s|{{CACHE_DB_NAME}}|cache-db|;
    s|{{CACHE_DB_USER}}|cache-db|;
    s|{{EDU_SERVER_FDQN}}|education.example.com|;
    s|{{EDU_SERVER_FDQN_TLS}}|education-example-com-tls|;
    s|{{B64_DB_PASSWORD}}|YWRtaW4=|;
    s|{{B64_CACHE_DB_PASSWORD}}|YWRtaW4==|" |
  kubectl -n staging apply -f -
```

### Install teacher-portal-ui

```shell
cat kubernetes/teacher-portal-ui.yaml |
  sed "s|{{TEACHER_PORTAL_UI_FDQN}}|teacher.example.com|;
    s|{{TEACHER_PORTAL_UI_FDQN_TLS}}|teacher-example-com-tls|" |
  kubectl -n staging apply -f -
```
