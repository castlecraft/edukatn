import {
  IsString,
  IsOptional,
  IsNumber,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

export class GetTopicDto {
  @IsString()
  @IsOptional()
  name: string;

  @IsNumber()
  @IsOptional()
  docstatus: number;

  @IsString()
  @IsOptional()
  topic_name: string;

  @IsString()
  @IsOptional()
  doctype: string;

  @ValidateNested()
  @Type(() => GetTopicContentDto)
  topic_content: GetTopicContentDto[];
}

export class GetTopicContentDto {
  @IsString()
  @IsOptional()
  name?: string;

  @IsNumber()
  @IsOptional()
  docstatus?: number;

  @IsString()
  @IsOptional()
  content_type: string;

  @IsString()
  @IsOptional()
  content: string;

  @IsString()
  @IsOptional()
  doctype?: string;
}
