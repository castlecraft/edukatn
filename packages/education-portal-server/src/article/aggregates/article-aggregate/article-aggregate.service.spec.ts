import { Test, TestingModule } from '@nestjs/testing';
import { ArticleAggregateService } from './article-aggregate.service';
import { ArticleService } from '../../entity/article/article.service';
import { TeacherService } from '../../../teacher/entity/teacher/teacher.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { HttpService } from '@nestjs/common';
import { TopicWebhookAggregateService } from '../../../topic/aggregates/topic-webhook-aggregate/topic-webhook-aggregate.service';
import { CourseService } from '../../../course/entity/course/course.service';
import { TopicService } from '../../../topic/entity/topic/topic.service';
import { ArticleWebhookAggregateService } from '../article-webhook-aggregate/article-webhook-aggregate.service';

describe('ArticleAggregateService', () => {
  let service: ArticleAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticleAggregateService,
        {
          provide: ArticleService,
          useValue: {},
        },
        {
          provide: TeacherService,
          useValue: {},
        },
        {
          provide: CourseService,
          useValue: {},
        },
        {
          provide: TopicService,
          useValue: {},
        },
        {
          provide: ArticleWebhookAggregateService,
          useValue: {},
        },
        {
          provide: TopicWebhookAggregateService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ArticleAggregateService>(ArticleAggregateService);
  });
  ArticleAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
