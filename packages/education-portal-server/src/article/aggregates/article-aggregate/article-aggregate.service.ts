import {
  Injectable,
  NotFoundException,
  HttpService,
  NotImplementedException,
  BadRequestException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { ArticleDto } from '../../entity/article/article-dto';
import { Article } from '../../entity/article/article.entity';
import { ArticleAddedEvent } from '../../event/article-added/article-added.event';
import { ArticleService } from '../../entity/article/article.service';
import { ArticleUpdatedEvent } from '../../event/article-updated/article-updated.event';
import { UpdateArticleDto } from '../../entity/article/update-article-dto';
import { TeacherService } from '../../../teacher/entity/teacher/teacher.service';
import { switchMap, catchError, map, mergeMap, toArray } from 'rxjs/operators';
import { throwError, from, of } from 'rxjs';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import {
  FRAPPE_API_GET_ARTICLE_ENDPOINT,
  FRAPPE_API_GET_TOPIC_ENDPOINT,
} from '../../../constants/routes';
import { ARTICLE, FORWARD_SLASH } from '../../../constants/app-strings';
import { GetTopicDto } from '../../../article/entity/article/get-topic-dto';
import { ServerSettings } from '../../../system-settings/entities/server-settings/server-settings.entity';
import { TopicWebhookDto } from '../../../topic/entity/topic/topic-webhook-dto';
import { TopicWebhookAggregateService } from '../../../topic/aggregates/topic-webhook-aggregate/topic-webhook-aggregate.service';
import { CourseService } from '../../../course/entity/course/course.service';
import { TopicService } from '../../../topic/entity/topic/topic.service';
import { TOPIC_NOT_FOUND, COURSE_NOT_FOUND } from '../../../constants/messages';
import { ArticleWebhookAggregateService } from '../article-webhook-aggregate/article-webhook-aggregate.service';
import { UpdateTopicStatusDto } from '../../../topic/entity/topic/update-topic-status-dto';

@Injectable()
export class ArticleAggregateService extends AggregateRoot {
  constructor(
    private readonly articleService: ArticleService,
    private readonly teacherService: TeacherService,
    private readonly articleWebhookService: ArticleWebhookAggregateService,
    private readonly courseService: CourseService,
    private readonly topicService: TopicService,
    private readonly topicWebhookService: TopicWebhookAggregateService,
    private readonly settings: SettingsService,
    private readonly http: HttpService,
  ) {
    super();
  }

  addArticle(articlePayload: ArticleDto, clientHttpRequest) {
    const article = new Article();
    Object.assign(article, articlePayload);
    article.uuid = uuidv4();
    this.apply(new ArticleAddedEvent(article, clientHttpRequest));
  }

  async retrieveArticle(uuid: string, req) {
    const provider = await this.articleService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  getArticleList(
    offset,
    limit,
    sort,
    search,
    clientHttpRequest,
    getArticleList: UpdateTopicStatusDto,
  ) {
    return from(
      this.teacherService.findOne({
        instructor_email: clientHttpRequest.token.email,
        'instructor_log.program': decodeURIComponent(
          getArticleList.program_name,
        ),
        'instructor_log.course': decodeURIComponent(getArticleList.course_name),
      }),
    ).pipe(
      switchMap(teacher => {
        if (!teacher) {
          return throwError(new NotFoundException());
        }
        return from(
          this.courseService.findOne({
            course_name: decodeURIComponent(getArticleList.course_name),
            'topics.topic_name': decodeURIComponent(getArticleList.topic_name),
          }),
        ).pipe(
          switchMap(course => {
            if (!course) {
              return throwError(new NotFoundException(COURSE_NOT_FOUND));
            }
            return this.getArticleName(
              decodeURIComponent(getArticleList.topic_name),
            ).pipe(
              switchMap(articleList => {
                return from(
                  this.articleService.list(
                    offset,
                    limit,
                    search,
                    sort,
                    articleList,
                  ),
                ).pipe(
                  switchMap(articles => {
                    return of(articles);
                  }),
                );
              }),
            );
          }),
        );
      }),
    );
  }

  getArticleName(topic_name: string) {
    return from(this.topicService.findOne({ name: topic_name })).pipe(
      mergeMap(topic => {
        if (!topic) {
          return throwError(new NotFoundException(TOPIC_NOT_FOUND));
        }
        return from(topic.topic_content).pipe(
          switchMap(topicList => {
            if (topicList.content_type === ARTICLE) {
              return of(topicList.content);
            }
            return of({});
          }),
        );
      }),
      toArray(),
    );
  }

  removeArticle(removeArticlePayload: UpdateArticleDto, clientHttpRequest) {
    return this.settings.find().pipe(
      switchMap(settings => {
        if (!settings) {
          return throwError(new NotImplementedException());
        }
        const url = `${settings.authServerURL}${FRAPPE_API_GET_TOPIC_ENDPOINT}${FORWARD_SLASH}${removeArticlePayload.topic_name}`;
        const headers = {
          headers: this.settings.getAuthorizationHeaders(
            clientHttpRequest.token,
          ),
        };
        return this.http.get(url, headers).pipe(
          map(res => res.data.data),
          switchMap(res => {
            res.topic_content.splice(
              res.topic_content.findIndex(
                x => x.content === UpdateArticleDto.name,
              ),
              1,
            );
            const body = {
              topic_content: res.topic_content,
            };
            return this.http.put(url, JSON.stringify(body), headers);
          }),
          map(res => res.data.data),
          switchMap(res => {
            return this.topicWebhookService.topicUpdated(res);
          }),
          catchError(err => {
            if (err.response && err.response.data) {
              return throwError(
                new BadRequestException(
                  this.settings.getErrorMessage(err.response.data),
                ),
              );
            } else {
              return throwError(new BadRequestException(err));
            }
          }),
        );
      }),
    );
  }

  async update(updatePayload: UpdateArticleDto) {
    const provider = await this.articleService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new ArticleUpdatedEvent(update));
  }

  updateSingleArticle(updatePayload: UpdateArticleDto, clientHttpRequest) {
    return this.settings.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(new NotImplementedException());
        }
        const url = `${settings.authServerURL}${FRAPPE_API_GET_ARTICLE_ENDPOINT}${FORWARD_SLASH}${updatePayload.name}`;
        const headers = {
          headers: this.settings.getAuthorizationHeaders(
            clientHttpRequest.token,
          ),
        };
        const body = {
          title: updatePayload.title,
          content: updatePayload.content,
        };
        return this.getArticleData(
          updatePayload,
          clientHttpRequest,
          settings,
        ).pipe(
          switchMap(() => {
            return this.http.put(url, JSON.stringify(body), headers);
          }),
          catchError(err => {
            if (err.response && err.response.data) {
              return throwError(
                new BadRequestException(
                  this.settings.getErrorMessage(err.response.data),
                ),
              );
            } else {
              return throwError(new BadRequestException(err));
            }
          }),
        );
      }),
      map(res => res.data.data),
      switchMap(article => {
        return this.articleWebhookService.articleUpdated(article);
      }),
    );
  }

  getArticleData(
    addArticlePayload: UpdateArticleDto,
    clientHttpRequest,
    settings: ServerSettings,
  ) {
    const url = `${settings.authServerURL}${FRAPPE_API_GET_ARTICLE_ENDPOINT}${FORWARD_SLASH}${addArticlePayload.name}`;
    const headers = {
      headers: this.settings.getAuthorizationHeaders(clientHttpRequest.token),
    };
    return this.http.get(url, headers).pipe(map(res => res.data.data));
  }

  getExistingArticle(addArticlePayload: UpdateArticleDto, clientHttpRequest) {
    return this.settings.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(new NotImplementedException());
        }
        return this.getArticleData(
          addArticlePayload,
          clientHttpRequest,
          settings,
        ).pipe(
          switchMap(() => {
            return this.addTopicArticle(
              addArticlePayload,
              clientHttpRequest,
              settings,
            );
          }),
          catchError(err => {
            if (
              err.response &&
              err.response.data &&
              err.response.status === 404
            ) {
              return this.addNewArticle(
                addArticlePayload,
                clientHttpRequest,
                settings,
              );
            } else {
              return throwError(
                new BadRequestException(
                  this.settings.getErrorMessage(err.response.data),
                ),
              );
            }
          }),
        );
      }),
    );
  }

  addNewArticle(
    addArticlePayload: UpdateArticleDto,
    clientHttpRequest,
    settings: ServerSettings,
  ) {
    const url = `${settings.authServerURL}${FRAPPE_API_GET_ARTICLE_ENDPOINT}`;
    const body = {
      name: addArticlePayload.name,
      title: addArticlePayload.name,
      content: addArticlePayload.content,
    };
    const headers = {
      headers: this.settings.getAuthorizationHeaders(clientHttpRequest.token),
    };
    return this.http.post(url, JSON.stringify(body), headers).pipe(
      map(res => res.data.data),
      switchMap((data: Article) => {
        this.articleService.create(data);
        return this.addTopicArticle(
          addArticlePayload,
          clientHttpRequest,
          settings,
        );
      }),
    );
  }

  addTopicArticle(
    addArticlePayload: UpdateArticleDto,
    clientHttpRequest,
    settings: ServerSettings,
  ) {
    const url = `${settings.authServerURL}${FRAPPE_API_GET_TOPIC_ENDPOINT}${FORWARD_SLASH}${addArticlePayload.topic_name}`;
    const headers = {
      headers: this.settings.getAuthorizationHeaders(clientHttpRequest.token),
    };
    return this.http.get(url, headers).pipe(
      map(res => res.data.data),
      switchMap((res: GetTopicDto) => {
        res.topic_content.push({
          content_type: ARTICLE,
          content: addArticlePayload.name,
        });
        const body = {
          topic_content: res.topic_content,
        };
        return this.http.put(url, JSON.stringify(body), headers);
      }),
      map(res => res.data.data),
      switchMap((data: TopicWebhookDto) => {
        return this.topicWebhookService.topicUpdated(data);
      }),
    );
  }
}
