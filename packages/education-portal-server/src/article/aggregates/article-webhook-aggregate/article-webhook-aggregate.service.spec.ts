import { Test, TestingModule } from '@nestjs/testing';
import { ArticleWebhookAggregateService } from './article-webhook-aggregate.service';
import { ArticleService } from '../../../article/entity/article/article.service';

describe('ArticleWebhookAggregateService', () => {
  let service: ArticleWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticleWebhookAggregateService,
        {
          provide: ArticleService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ArticleWebhookAggregateService>(
      ArticleWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
