import { Injectable, BadRequestException } from '@nestjs/common';
import { ArticleWebhookDto } from '../../entity/article/article-webhook-dto';
import { ArticleService } from '../../../article/entity/article/article.service';
import { Article } from '../../../article/entity/article/article.entity';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ARTICLE_ALREADY_EXISTS } from '../../../constants/messages';
import * as uuidv4 from 'uuid/v4';
@Injectable()
export class ArticleWebhookAggregateService {
  constructor(private readonly articleService: ArticleService) {}

  articleCreated(articlePayload: ArticleWebhookDto) {
    return from(
      this.articleService.findOne({
        name: articlePayload.name,
      }),
    ).pipe(
      switchMap(article => {
        if (article) {
          return throwError(new BadRequestException(ARTICLE_ALREADY_EXISTS));
        }
        const provider = this.mapArticle(articlePayload);
        return from(this.articleService.create(provider)).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }

  mapArticle(articlePayload: ArticleWebhookDto) {
    const article = new Article();
    Object.assign(article, articlePayload);
    article.isSynced = true;
    article.uuid = uuidv4();
    return article;
  }

  articleDeleted(articlePayload: ArticleWebhookDto) {
    this.articleService.deleteOne({ name: articlePayload.name });
  }

  articleUpdated(articlePayload: ArticleWebhookDto) {
    return from(
      this.articleService.findOne({ name: articlePayload.name }),
    ).pipe(
      switchMap(article => {
        if (!article) {
          return this.articleCreated(articlePayload);
        }
        article.isSynced = true;
        return from(
          this.articleService.updateOne(
            { name: article.name },
            { $set: articlePayload },
          ),
        ).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }
}
