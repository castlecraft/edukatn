import { RetrieveArticleQueryHandler } from './get-article/retrieve-article.handler';
import { RetrieveArticleListQueryHandler } from './list-article/retrieve-article-list.handler';

export const ArticleQueryManager = [
  RetrieveArticleQueryHandler,
  RetrieveArticleListQueryHandler,
];
