import { ArticleAddedCommandHandler } from './article-added/article-added.handler';
import { ArticleRemovedCommandHandler } from './article-removed/article.removed.handler';
import { ArticleUpdatedCommandHandler } from './article-updated/article-updated.handler';

export const ArticleEventManager = [
  ArticleAddedCommandHandler,
  ArticleRemovedCommandHandler,
  ArticleUpdatedCommandHandler,
];
