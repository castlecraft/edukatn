import { ICommand } from '@nestjs/cqrs';
import { UpdateArticleDto } from '../../entity/article/update-article-dto';

export class UpdateSingleArticleCommand implements ICommand {
  constructor(
    public readonly updatePayload: UpdateArticleDto,
    public readonly req,
  ) {}
}
