import { ICommand } from '@nestjs/cqrs';
import { UpdateArticleDto } from '../../entity/article/update-article-dto';

export class AddNewArticleCommand implements ICommand {
  constructor(
    public addArticlePayload: UpdateArticleDto,
    public readonly clientHttpRequest: any,
  ) {}
}
