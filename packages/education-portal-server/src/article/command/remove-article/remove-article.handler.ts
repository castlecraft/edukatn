import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveArticleCommand } from './remove-article.command';
import { ArticleAggregateService } from '../../aggregates/article-aggregate/article-aggregate.service';

@CommandHandler(RemoveArticleCommand)
export class RemoveArticleCommandHandler
  implements ICommandHandler<RemoveArticleCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: ArticleAggregateService,
  ) {}
  async execute(command: RemoveArticleCommand) {
    const { removeArticlePayload, req } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.removeArticle(removeArticlePayload, req).toPromise();
    aggregate.commit();
  }
}
