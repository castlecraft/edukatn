import { ICommand } from '@nestjs/cqrs';
import { UpdateArticleDto } from '../../entity/article/update-article-dto';

export class UpdateArticleCommand implements ICommand {
  constructor(public readonly updatePayload: UpdateArticleDto) {}
}
