import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveCourseScheduleCommand } from './remove-course-schedule.command';
import { CourseScheduleAggregateService } from '../../aggregates/course-schedule-aggregate/course-schedule-aggregate.service';

@CommandHandler(RemoveCourseScheduleCommand)
export class RemoveCourseScheduleCommandHandler
  implements ICommandHandler<RemoveCourseScheduleCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: CourseScheduleAggregateService,
  ) {}
  async execute(command: RemoveCourseScheduleCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.remove(uuid);
    aggregate.commit();
  }
}
