import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddCourseScheduleCommand } from './add-course-schedule.command';
import { CourseScheduleAggregateService } from '../../aggregates/course-schedule-aggregate/course-schedule-aggregate.service';

@CommandHandler(AddCourseScheduleCommand)
export class AddCourseScheduleCommandHandler
  implements ICommandHandler<AddCourseScheduleCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: CourseScheduleAggregateService,
  ) {}
  async execute(command: AddCourseScheduleCommand) {
    const { courseSchedulePayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addCourseSchedule(courseSchedulePayload, clientHttpRequest);
    aggregate.commit();
  }
}
