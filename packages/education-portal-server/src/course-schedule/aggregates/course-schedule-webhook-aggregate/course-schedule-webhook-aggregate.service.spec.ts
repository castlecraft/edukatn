import { Test, TestingModule } from '@nestjs/testing';
import { CourseScheduleWebhookAggregateService } from './course-schedule-webhook-aggregate.service';
import { CourseScheduleService } from '../../../course-schedule/entity/course-schedule/course-schedule.service';

describe('CourseScheduleWebhookAggregateService', () => {
  let service: CourseScheduleWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CourseScheduleWebhookAggregateService,
        {
          provide: CourseScheduleService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<CourseScheduleWebhookAggregateService>(
      CourseScheduleWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
