import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { CourseScheduleUpdatedEvent } from './course-schedule-updated.event';
import { CourseScheduleService } from '../../entity/course-schedule/course-schedule.service';

@EventsHandler(CourseScheduleUpdatedEvent)
export class CourseScheduleUpdatedCommandHandler
  implements IEventHandler<CourseScheduleUpdatedEvent> {
  constructor(private readonly object: CourseScheduleService) {}

  async handle(event: CourseScheduleUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}
