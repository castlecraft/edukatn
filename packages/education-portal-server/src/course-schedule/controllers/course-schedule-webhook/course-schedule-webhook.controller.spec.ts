import { Test, TestingModule } from '@nestjs/testing';
import { CourseScheduleWebhookController } from './course-schedule-webhook.controller';
import { CourseScheduleWebhookAggregateService } from '../../aggregates/course-schedule-webhook-aggregate/course-schedule-webhook-aggregate.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

describe('CourseScheduleWebhook Controller', () => {
  let controller: CourseScheduleWebhookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: CourseScheduleWebhookAggregateService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
      ],
      controllers: [CourseScheduleWebhookController],
    }).compile();

    controller = module.get<CourseScheduleWebhookController>(
      CourseScheduleWebhookController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
