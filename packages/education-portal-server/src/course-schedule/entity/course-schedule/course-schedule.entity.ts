import { Column, ObjectIdColumn, BaseEntity, ObjectID, Entity } from 'typeorm';

@Entity()
export class CourseSchedule extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  name: string;

  @Column()
  docstatus: number;

  @Column()
  student_group: string;

  @Column()
  instructor: string;

  @Column()
  instructor_name: string;

  @Column()
  naming_series: string;

  @Column()
  course: string;

  @Column()
  schedule_date: string;

  @Column()
  room: string;

  @Column()
  from_time: string;

  @Column()
  to_time: string;

  @Column()
  title: string;

  @Column()
  doctype: string;

  @Column()
  isSynced: boolean;
}
