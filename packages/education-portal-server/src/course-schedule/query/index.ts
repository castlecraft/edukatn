import { RetrieveCourseScheduleQueryHandler } from './get-course-schedule/retrieve-course-schedule.handler';
import { RetrieveCourseScheduleListQueryHandler } from './list-course-schedule/retrieve-course-schedule-list.handler';

export const CourseScheduleQueryManager = [
  RetrieveCourseScheduleQueryHandler,
  RetrieveCourseScheduleListQueryHandler,
];
