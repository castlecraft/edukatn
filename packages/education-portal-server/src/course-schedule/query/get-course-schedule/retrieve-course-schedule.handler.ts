import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveCourseScheduleQuery } from './retrieve-course-schedule.query';
import { CourseScheduleAggregateService } from '../../aggregates/course-schedule-aggregate/course-schedule-aggregate.service';

@QueryHandler(RetrieveCourseScheduleQuery)
export class RetrieveCourseScheduleQueryHandler
  implements IQueryHandler<RetrieveCourseScheduleQuery> {
  constructor(private readonly manager: CourseScheduleAggregateService) {}

  async execute(query: RetrieveCourseScheduleQuery) {
    const { req, uuid } = query;
    return this.manager.retrieveCourseSchedule(uuid, req);
  }
}
