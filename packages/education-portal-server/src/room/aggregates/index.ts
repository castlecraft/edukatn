import { RoomAggregateService } from './room-aggregate/room-aggregate.service';
import { RoomWebhookAggregateService } from './room-webhook-aggregate/room-webhook-aggregate.service';

export const RoomAggregatesManager = [
  RoomAggregateService,
  RoomWebhookAggregateService,
];
