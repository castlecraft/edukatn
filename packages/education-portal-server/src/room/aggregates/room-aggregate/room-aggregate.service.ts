import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { RoomDto } from '../../entity/room/room-dto';
import { Room } from '../../entity/room/room.entity';
import { RoomAddedEvent } from '../../event/room-added/room-added.event';
import { RoomService } from '../../entity/room/room.service';
import { RoomRemovedEvent } from '../../event/room-removed/room-removed.event';
import { RoomUpdatedEvent } from '../../event/room-updated/room-updated.event';
import { UpdateRoomDto } from '../../entity/room/update-room-dto';

@Injectable()
export class RoomAggregateService extends AggregateRoot {
  constructor(private readonly roomService: RoomService) {
    super();
  }

  addRoom(roomPayload: RoomDto, clientHttpRequest) {
    const room = new Room();
    Object.assign(room, roomPayload);
    room.uuid = uuidv4();
    this.apply(new RoomAddedEvent(room, clientHttpRequest));
  }

  async retrieveRoom(uuid: string, req) {
    const provider = await this.roomService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getRoomList(offset, limit, sort, search, clientHttpRequest) {
    return await this.roomService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.roomService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new RoomRemovedEvent(found));
  }

  async update(updatePayload: UpdateRoomDto) {
    const provider = await this.roomService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new RoomUpdatedEvent(update));
  }
}
