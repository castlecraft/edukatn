import { IsString, IsOptional, IsNumber } from 'class-validator';

export class RoomWebhookDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  room_name: string;

  @IsOptional()
  @IsString()
  room_number: string;

  @IsOptional()
  @IsString()
  seating_capacity: string;

  @IsOptional()
  @IsString()
  doctype: string;
}
