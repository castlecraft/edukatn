import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateRoomCommand } from './update-room.command';
import { RoomAggregateService } from '../../aggregates/room-aggregate/room-aggregate.service';

@CommandHandler(UpdateRoomCommand)
export class UpdateRoomCommandHandler
  implements ICommandHandler<UpdateRoomCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: RoomAggregateService,
  ) {}

  async execute(command: UpdateRoomCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.update(updatePayload);
    aggregate.commit();
  }
}
