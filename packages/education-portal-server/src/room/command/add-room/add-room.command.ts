import { ICommand } from '@nestjs/cqrs';
import { RoomDto } from '../../entity/room/room-dto';

export class AddRoomCommand implements ICommand {
  constructor(
    public roomPayload: RoomDto,
    public readonly clientHttpRequest: any,
  ) {}
}
