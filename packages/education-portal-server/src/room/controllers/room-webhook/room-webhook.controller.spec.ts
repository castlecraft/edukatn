import { Test, TestingModule } from '@nestjs/testing';
import { RoomWebhookController } from './room-webhook.controller';
import { RoomWebhookAggregateService } from '../../../room/aggregates/room-webhook-aggregate/room-webhook-aggregate.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

describe('RoomWebhook Controller', () => {
  let controller: RoomWebhookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: RoomWebhookAggregateService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
      ],
      controllers: [RoomWebhookController],
    }).compile();

    controller = module.get<RoomWebhookController>(RoomWebhookController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
