import { IQuery } from '@nestjs/cqrs';
import { GetCourseQueryDto } from '../../../constants/listing-dto/get-course-dto';

export class RetrieveCourseQuery implements IQuery {
  constructor(
    public readonly getCoursePayload: GetCourseQueryDto,
    public readonly req: any,
  ) {}
}
