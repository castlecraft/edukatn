import { Test, TestingModule } from '@nestjs/testing';
import { CourseWebhookAggregateService } from './course-webhook-aggregate.service';
import { CourseService } from '../../entity/course/course.service';

describe('CourseWebhookAggregateService', () => {
  let service: CourseWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CourseWebhookAggregateService,
        {
          provide: CourseService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<CourseWebhookAggregateService>(
      CourseWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
