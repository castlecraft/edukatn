import { Injectable, BadRequestException } from '@nestjs/common';
import { CourseWebhookDto } from '../../entity/course/course-webhook-dto';
import { CourseService } from '../../entity/course/course.service';
import { Course } from '../../entity/course/course.entity';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { COURSE_ALREADY_EXISTS } from '../../../constants/messages';
import * as uuidv4 from 'uuid/v4';
@Injectable()
export class CourseWebhookAggregateService {
  constructor(private readonly courseService: CourseService) {}

  courseCreated(coursePayload: CourseWebhookDto) {
    return from(
      this.courseService.findOne({
        name: coursePayload.name,
      }),
    ).pipe(
      switchMap(course => {
        if (course) {
          return throwError(new BadRequestException(COURSE_ALREADY_EXISTS));
        }
        const provider = this.mapCourse(coursePayload);
        return from(this.courseService.create(provider)).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }

  mapCourse(coursePayload: CourseWebhookDto) {
    const course = new Course();
    Object.assign(course, coursePayload);
    course.uuid = uuidv4();
    course.isSynced = true;
    course.topics.forEach(element => {
      element.status = 'Remaining';
    });
    return course;
  }

  courseDeleted(coursePayload: CourseWebhookDto) {
    this.courseService.deleteOne({ name: coursePayload.name });
  }

  courseUpdated(coursePayload: CourseWebhookDto) {
    return from(this.courseService.findOne({ name: coursePayload.name })).pipe(
      switchMap(course => {
        if (!course) {
          return this.courseCreated(coursePayload);
        }
        course = this.getTopicsPayload(coursePayload, course);
        return from(
          this.courseService.updateOne({ name: course.name }, { $set: course }),
        );
      }),
    );
  }

  getTopicsPayload(webhookPayload: CourseWebhookDto, course: Course) {
    const topicsMap = {};
    if (webhookPayload.topics.length < course.topics.length) {
      webhookPayload.topics.forEach(topic => {
        topicsMap[topic.name] = topic;
      });
      course.topics = course.topics.filter(topic => {
        if (
          topicsMap[topic.name] &&
          topic.name === topicsMap[topic.name].name
        ) {
          return topic;
        }
      });
    } else {
      webhookPayload.topics.forEach(topic => {
        topicsMap[topic.name] = topic;
      });
      course.topics.forEach(topic => {
        delete topicsMap[topic.name];
      });
      for (const key of Object.keys(topicsMap)) {
        topicsMap[key].status = 'Remaining';
        course.topics.push(topicsMap[key]);
      }
    }
    course.isSynced = true;
    return course;
  }
}
