import {
  IsString,
  IsOptional,
  IsNumber,
  ValidateNested,
  IsEnum,
} from 'class-validator';
import { Type } from 'class-transformer';
import { TOPICS_STATUS_ENUM } from '../../../constants/app-strings';

export class CourseDto {
  @IsString()
  @IsOptional()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsString()
  @IsOptional()
  course_name: string;

  @IsString()
  @IsOptional()
  department: string;

  @IsString()
  @IsOptional()
  doctype: string;

  @ValidateNested()
  @Type(() => CourseTopicsDto)
  topics: CourseTopicsDto[];
}
export class CourseTopicsDto {
  @IsString()
  @IsOptional()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsString()
  @IsOptional()
  topic: string;

  @IsString()
  @IsOptional()
  topic_name: string;

  @IsString()
  @IsOptional()
  doctype: string;

  @IsString()
  @IsOptional()
  @IsEnum(TOPICS_STATUS_ENUM)
  status: string;

  @IsString()
  @IsOptional()
  completed_date: string;
}
