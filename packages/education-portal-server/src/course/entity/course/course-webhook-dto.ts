import {
  IsString,
  IsOptional,
  IsNumber,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

export class CourseWebhookDto {
  @IsString()
  @IsOptional()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsString()
  @IsOptional()
  course_name: string;

  @IsString()
  @IsOptional()
  department: string;

  @IsString()
  @IsOptional()
  doctype: string;

  @ValidateNested()
  @Type(() => CourseTopicsWebhookDto)
  topics: CourseTopicsWebhookDto[];
}
export class CourseTopicsWebhookDto {
  @IsString()
  @IsOptional()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsString()
  @IsOptional()
  topic: string;

  @IsString()
  @IsOptional()
  topic_name: string;

  @IsString()
  @IsOptional()
  doctype: string;

  @IsString()
  @IsOptional()
  status: string;

  @IsString()
  @IsOptional()
  completed_date: string;
}
