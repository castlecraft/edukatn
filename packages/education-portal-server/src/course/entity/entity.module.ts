import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Course } from './course/course.entity';
import { CourseService } from './course/course.service';
import { CqrsModule } from '@nestjs/cqrs';
import { CourseWebhookAggregateService } from '../aggregates/course-webhook-aggregate/course-webhook-aggregate.service';

@Module({
  imports: [TypeOrmModule.forFeature([Course]), CqrsModule],
  providers: [CourseService, CourseWebhookAggregateService],
  exports: [CourseService, CourseWebhookAggregateService],
})
export class CourseEntitiesModule {}
