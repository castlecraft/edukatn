import { Module, HttpModule } from '@nestjs/common';
import { CourseAggregatesManager } from './aggregates';
import { CourseEntitiesModule } from './entity/entity.module';
import { CourseQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { CourseCommandManager } from './command';
import { CourseEventManager } from './event';
import { CourseController } from './controllers/course/course.controller';
import { CoursePoliciesService } from './policies/course-policies/course-policies.service';
import { CourseWebhookController } from './controllers/course-webhook/course-webhook.controller';
import { TeacherEntitiesModule } from '../teacher/entity/entity.module';

@Module({
  imports: [
    CourseEntitiesModule,
    TeacherEntitiesModule,
    CqrsModule,
    HttpModule,
  ],
  controllers: [CourseController, CourseWebhookController],
  providers: [
    ...CourseAggregatesManager,
    ...CourseQueryManager,
    ...CourseEventManager,
    ...CourseCommandManager,
    CoursePoliciesService,
  ],
  exports: [CourseEntitiesModule],
})
export class CourseModule {}
