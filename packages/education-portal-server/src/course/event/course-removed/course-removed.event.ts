import { IEvent } from '@nestjs/cqrs';
import { Course } from '../../entity/course/course.entity';

export class CourseRemovedEvent implements IEvent {
  constructor(public course: Course) {}
}
