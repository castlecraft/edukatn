import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CourseDto } from '../../entity/course/course-dto';
import { AddCourseCommand } from '../../command/add-course/add-course.command';
import { RemoveCourseCommand } from '../../command/remove-course/remove-course.command';
import { UpdateCourseCommand } from '../../command/update-course/update-course.command';
import { RetrieveCourseQuery } from '../../query/get-course/retrieve-course.query';
import { RetrieveCourseListQuery } from '../../query/list-course/retrieve-course-list.query';
import { UpdateCourseDto } from '../../entity/course/update-course-dto';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { GetCourseQueryDto } from '../../../constants/listing-dto/get-course-dto';

@Controller('course')
export class CourseController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() coursePayload: CourseDto, @Req() req) {
    return this.commandBus.execute(new AddCourseCommand(coursePayload, req));
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid: string) {
    return this.commandBus.execute(new RemoveCourseCommand(uuid));
  }

  @Get('v1/get')
  @UseGuards(TokenGuard)
  getCourse(@Query() getCoursePayload: GetCourseQueryDto, @Req() req) {
    return this.queryBus.execute(
      new RetrieveCourseQuery(getCoursePayload, req),
    );
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getCourseList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Req() clientHttpRequest,
  ) {
    if (sort !== 'ASC') {
      sort = -1;
    } else {
      sort = 1;
    }

    return this.queryBus.execute(
      new RetrieveCourseListQuery(
        offset,
        limit,
        sort,
        search,
        clientHttpRequest,
      ),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateCourse(@Body() updatePayload: UpdateCourseDto) {
    return this.commandBus.execute(new UpdateCourseCommand(updatePayload));
  }
}
