import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateCourseCommand } from './update-course.command';
import { CourseAggregateService } from '../../aggregates/course-aggregate/course-aggregate.service';

@CommandHandler(UpdateCourseCommand)
export class UpdateCourseCommandHandler
  implements ICommandHandler<UpdateCourseCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: CourseAggregateService,
  ) {}

  async execute(command: UpdateCourseCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.update(updatePayload);
    aggregate.commit();
  }
}
