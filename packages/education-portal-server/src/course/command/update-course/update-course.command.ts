import { ICommand } from '@nestjs/cqrs';
import { UpdateCourseDto } from '../../entity/course/update-course-dto';

export class UpdateCourseCommand implements ICommand {
  constructor(public readonly updatePayload: UpdateCourseDto) {}
}
