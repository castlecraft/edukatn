import { IsOptional, IsString, IsNumber } from 'class-validator';

export class VideoWebhookDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  title: string;

  @IsOptional()
  @IsString()
  provider: string;

  @IsOptional()
  @IsString()
  url: string;

  @IsOptional()
  @IsString()
  publish_date: string;

  @IsOptional()
  @IsString()
  duration: string;

  @IsOptional()
  @IsString()
  description: string;

  @IsOptional()
  @IsString()
  doctype: string;
}
