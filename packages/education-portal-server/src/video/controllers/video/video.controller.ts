import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { VideoDto } from '../../entity/video/video-dto';
import { AddVideoCommand } from '../../command/add-video/add-video.command';
import { RemoveVideoCommand } from '../../command/remove-video/remove-video.command';
import { UpdateVideoCommand } from '../../command/update-video/update-video.command';
import { RetrieveVideoQuery } from '../../query/get-video/retrieve-video.query';
import { RetrieveVideoListQuery } from '../../query/list-video/retrieve-video-list.query';
import { UpdateVideoDto } from '../../entity/video/update-video-dto';
import { GetCourseQueryDto } from '../../../constants/listing-dto/get-course-dto';

@Controller('video')
export class VideoController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() videoPayload: VideoDto, @Req() req) {
    return this.commandBus.execute(new AddVideoCommand(videoPayload, req));
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid: string) {
    return this.commandBus.execute(new RemoveVideoCommand(uuid));
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getClient(@Param('uuid') uuid: string, @Req() req) {
    return await this.queryBus.execute(new RetrieveVideoQuery(uuid, req));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getClientList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Query() getVideoPayload: GetCourseQueryDto,
    @Req() clientHttpRequest,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveVideoListQuery(
        offset,
        limit,
        sort,
        search,
        getVideoPayload,
        clientHttpRequest,
      ),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateClient(@Body() updatePayload: UpdateVideoDto) {
    return this.commandBus.execute(new UpdateVideoCommand(updatePayload));
  }
}
