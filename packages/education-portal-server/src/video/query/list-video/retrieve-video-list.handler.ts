import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveVideoListQuery } from './retrieve-video-list.query';
import { VideoAggregateService } from '../../aggregates/video-aggregate/video-aggregate.service';

@QueryHandler(RetrieveVideoListQuery)
export class RetrieveVideoListQueryHandler
  implements IQueryHandler<RetrieveVideoListQuery> {
  constructor(private readonly manager: VideoAggregateService) {}
  async execute(query: RetrieveVideoListQuery) {
    const {
      offset,
      getVideoPayload,
      limit,
      search,
      sort,
      clientHttpRequest,
    } = query;
    return await this.manager
      .getVideoList(
        Number(offset),
        Number(limit),
        search,
        sort,
        getVideoPayload,
        clientHttpRequest,
      )
      .toPromise();
  }
}
