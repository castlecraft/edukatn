import { RetrieveVideoQueryHandler } from './get-video/retrieve-video.handler';
import { RetrieveVideoListQueryHandler } from './list-video/retrieve-video-list.handler';

export const VideoQueryManager = [
  RetrieveVideoQueryHandler,
  RetrieveVideoListQueryHandler,
];
