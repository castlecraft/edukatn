import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveVideoCommand } from './remove-video.command';
import { VideoAggregateService } from '../../aggregates/video-aggregate/video-aggregate.service';

@CommandHandler(RemoveVideoCommand)
export class RemoveVideoCommandHandler
  implements ICommandHandler<RemoveVideoCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: VideoAggregateService,
  ) {}
  async execute(command: RemoveVideoCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.remove(uuid);
    aggregate.commit();
  }
}
