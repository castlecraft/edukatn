import { AddVideoCommandHandler } from './add-video/add-video.handler';
import { RemoveVideoCommandHandler } from './remove-video/remove-video.handler';
import { UpdateVideoCommandHandler } from './update-video/update-video.handler';

export const VideoCommandManager = [
  AddVideoCommandHandler,
  RemoveVideoCommandHandler,
  UpdateVideoCommandHandler,
];
