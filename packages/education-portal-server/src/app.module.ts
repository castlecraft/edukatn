import { Module, HttpModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from './config/config.module';
import { AuthModule } from './auth/auth.module';
import { SystemSettingsModule } from './system-settings/system-settings.module';
import {
  connectTypeORM,
  connectTypeORMTokenCache,
  TOKEN_CACHE_CONNECTION,
  DEFAULT,
} from './constants/typeorm.connection';
import { ConfigService } from './config/config.service';
import { TerminusModule } from '@nestjs/terminus';
import { TerminusOptionsService } from './system-settings/aggregates/terminus-options/terminus-options.service';
import { DirectModule } from './direct/direct.module';
import { TeacherModule } from './teacher/teacher.module';
import { StudentModule } from './student/student.module';
import { ProgramModule } from './program/program.module';
import { CourseModule } from './course/course.module';
import { ArticleModule } from './article/article.module';
import { TopicModule } from './topic/topic.module';
import { VideoModule } from './video/video.module';
import { QuizModule } from './quiz/quiz.module';
import { QuestionModule } from './question/question.module';
import { DepartmentModule } from './department/department.module';
import { AttendanceModule } from './attendance/attendance.module';
import { StudentGroupModule } from './student-group/student-group.module';
import { RoomModule } from './room/room.module';
import { CourseScheduleModule } from './course-schedule/course-schedule.module';

@Module({
  imports: [
    CourseScheduleModule,
    RoomModule,
    StudentGroupModule,
    AttendanceModule,
    DepartmentModule,
    QuestionModule,
    QuizModule,
    ArticleModule,
    VideoModule,
    TopicModule,
    CourseModule,
    ProgramModule,
    TeacherModule,
    StudentModule,
    HttpModule,
    TypeOrmModule.forRootAsync({
      name: TOKEN_CACHE_CONNECTION,
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: connectTypeORMTokenCache,
    }),
    TypeOrmModule.forRootAsync({
      name: DEFAULT,
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: connectTypeORM,
    }),
    TerminusModule.forRootAsync({ useClass: TerminusOptionsService }),
    ConfigModule,
    AuthModule,
    SystemSettingsModule,
    DirectModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
