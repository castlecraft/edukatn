import { Module, HttpModule } from '@nestjs/common';
import { StudentGroupAggregatesManager } from './aggregates';
import { StudentGroupEntitiesModule } from './entity/entity.module';
import { StudentGroupQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { StudentGroupCommandManager } from './command';
import { StudentGroupEventManager } from './event';
import { StudentGroupController } from './controllers/student-group/student-group.controller';
import { StudentGroupPoliciesService } from './policies/student-group-policies/student-group-policies.service';
import { StudentGroupWebhookController } from '../student-group/controllers/student-group-webhook/student-group-webhook.controller';

@Module({
  imports: [StudentGroupEntitiesModule, CqrsModule, HttpModule],
  controllers: [StudentGroupController, StudentGroupWebhookController],
  providers: [
    ...StudentGroupAggregatesManager,
    ...StudentGroupQueryManager,
    ...StudentGroupEventManager,
    ...StudentGroupCommandManager,
    StudentGroupPoliciesService,
  ],
  exports: [StudentGroupEntitiesModule],
})
export class StudentGroupModule {}
