import { Test, TestingModule } from '@nestjs/testing';
import { StudentGroupWebhookController } from './student-group-webhook.controller';
import { StudentGroupWebhookAggregateService } from '../../aggregates/student-group-webhook-aggregate/student-group-webhook-aggregate.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

describe('StudentGroupWebhook Controller', () => {
  let controller: StudentGroupWebhookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: StudentGroupWebhookAggregateService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
      ],
      controllers: [StudentGroupWebhookController],
    }).compile();

    controller = module.get<StudentGroupWebhookController>(
      StudentGroupWebhookController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
