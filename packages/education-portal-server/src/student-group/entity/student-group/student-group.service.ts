import { InjectRepository } from '@nestjs/typeorm';
import { StudentGroup } from './student-group.entity';
import { Injectable } from '@nestjs/common';
import { MongoRepository } from 'typeorm';

@Injectable()
export class StudentGroupService {
  constructor(
    @InjectRepository(StudentGroup)
    private readonly studentGroupRepository: MongoRepository<StudentGroup>,
  ) {}

  async find(query?) {
    return await this.studentGroupRepository.find(query);
  }

  async create(studentGroup: StudentGroup) {
    const studentGroupObject = new StudentGroup();
    Object.assign(studentGroupObject, studentGroup);
    return await this.studentGroupRepository.insertOne(studentGroupObject);
  }

  async findOne(param, options?) {
    return await this.studentGroupRepository.findOne(param, options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.studentGroupRepository.manager.connection
      .getMetadata(StudentGroup)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.studentGroupRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.studentGroupRepository.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, options?) {
    return await this.studentGroupRepository.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.studentGroupRepository.updateOne(query, options);
  }
}
