import {
  IsOptional,
  IsString,
  IsNumber,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

export class StudentGroupWebhookDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  academic_year: string;

  @IsOptional()
  @IsString()
  group_based_on: string;

  @IsOptional()
  @IsString()
  student_group_name: string;

  @IsOptional()
  @IsNumber()
  max_strength: number;

  @IsOptional()
  @IsString()
  academic_term: string;

  @IsOptional()
  @IsString()
  program: string;

  @IsOptional()
  @IsString()
  course: string;

  @IsOptional()
  @IsNumber()
  disabled: number;

  @IsOptional()
  @IsString()
  doctype: string;

  @ValidateNested()
  @Type(() => GroupStudentsWebhookDto)
  students: GroupStudentsWebhookDto[];

  @ValidateNested()
  @Type(() => InsructorsWebhookDto)
  instructors: InsructorsWebhookDto[];
}
export class GroupStudentsWebhookDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  student: string;

  @IsOptional()
  @IsString()
  student_name: string;

  @IsOptional()
  @IsNumber()
  group_roll_number: number;

  @IsOptional()
  @IsNumber()
  active: number;

  @IsOptional()
  @IsString()
  doctype: string;
}
export class InsructorsWebhookDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  instructor: string;

  @IsOptional()
  @IsString()
  instructor_name: string;

  @IsOptional()
  @IsString()
  doctype: string;
}
