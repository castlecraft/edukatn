import { ICommand } from '@nestjs/cqrs';
import { UpdateStudentGroupDto } from '../../entity/student-group/update-student-group-dto';

export class UpdateStudentGroupCommand implements ICommand {
  constructor(public readonly updatePayload: UpdateStudentGroupDto) {}
}
