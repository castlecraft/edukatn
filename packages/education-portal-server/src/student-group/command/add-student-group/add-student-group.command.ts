import { ICommand } from '@nestjs/cqrs';
import { StudentGroupDto } from '../../entity/student-group/student-group-dto';

export class AddStudentGroupCommand implements ICommand {
  constructor(
    public studentGroupPayload: StudentGroupDto,
    public readonly clientHttpRequest: any,
  ) {}
}
