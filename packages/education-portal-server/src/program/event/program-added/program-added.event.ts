import { IEvent } from '@nestjs/cqrs';
import { Program } from '../../entity/program/program.entity';

export class ProgramAddedEvent implements IEvent {
  constructor(public program: Program, public clientHttpRequest: any) {}
}
