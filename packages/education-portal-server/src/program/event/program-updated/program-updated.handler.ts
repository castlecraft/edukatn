import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ProgramUpdatedEvent } from './program-updated.event';
import { ProgramService } from '../../entity/program/program.service';

@EventsHandler(ProgramUpdatedEvent)
export class ProgramUpdatedCommandHandler
  implements IEventHandler<ProgramUpdatedEvent> {
  constructor(private readonly object: ProgramService) {}

  async handle(event: ProgramUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}
