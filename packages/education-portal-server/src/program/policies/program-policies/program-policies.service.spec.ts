import { Test, TestingModule } from '@nestjs/testing';
import { ProgramPoliciesService } from './program-policies.service';

describe('ProgramPoliciesService', () => {
  let service: ProgramPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProgramPoliciesService],
    }).compile();

    service = module.get<ProgramPoliciesService>(ProgramPoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
