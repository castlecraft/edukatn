import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { ProgramDto } from '../../entity/program/program-dto';
import { Program } from '../../entity/program/program.entity';
import { ProgramAddedEvent } from '../../event/program-added/program-added.event';
import { ProgramService } from '../../entity/program/program.service';
import { ProgramRemovedEvent } from '../../event/program-removed/program-removed.event';
import { ProgramUpdatedEvent } from '../../event/program-updated/program-updated.event';
import { UpdateProgramDto } from '../../entity/program/update-program-dto';

@Injectable()
export class ProgramAggregateService extends AggregateRoot {
  constructor(private readonly programService: ProgramService) {
    super();
  }

  addProgram(programPayload: ProgramDto, clientHttpRequest) {
    const program = new Program();
    Object.assign(program, programPayload);
    program.uuid = uuidv4();
    this.apply(new ProgramAddedEvent(program, clientHttpRequest));
  }

  async retrieveProgram(uuid: string, req) {
    const provider = await this.programService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getProgramList(offset, limit, sort, search, clientHttpRequest) {
    return await this.programService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.programService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new ProgramRemovedEvent(found));
  }

  async update(updatePayload: UpdateProgramDto) {
    const provider = await this.programService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new ProgramUpdatedEvent(update));
  }
}
