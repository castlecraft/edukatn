import { Test, TestingModule } from '@nestjs/testing';
import { ProgramWebhookAggregateService } from './program-webhook-aggregate.service';
import { ProgramService } from '../../entity/program/program.service';

describe('ProgramWebhookAggregateService', () => {
  let service: ProgramWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProgramWebhookAggregateService,
        {
          provide: ProgramService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ProgramWebhookAggregateService>(
      ProgramWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
