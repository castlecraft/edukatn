import { InjectRepository } from '@nestjs/typeorm';
import { Program } from './program.entity';
import { Injectable } from '@nestjs/common';
import { MongoRepository } from 'typeorm';

@Injectable()
export class ProgramService {
  constructor(
    @InjectRepository(Program)
    private readonly programRepository: MongoRepository<Program>,
  ) {}

  async find(query?) {
    return await this.programRepository.find(query);
  }

  async create(program: Program) {
    const programObject = new Program();
    Object.assign(programObject, program);
    return await this.programRepository.insertOne(programObject);
  }

  async findOne(param, options?) {
    return await this.programRepository.findOne(param, options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.programRepository.manager.connection
      .getMetadata(Program)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.programRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.programRepository.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, options?) {
    return await this.programRepository.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.programRepository.updateOne(query, options);
  }
}
