import {
  Controller,
  Post,
  Body,
  UsePipes,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { QuizWebhookDto } from '../../entity/quiz/quiz-webhook-dto';
import { QuizWebhookAggregateService } from '../../aggregates/quiz-webhook-aggregate/quiz-webhook-aggregate.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';

@Controller('quiz')
export class QuizWebhookController {
  constructor(
    private readonly quizWebhookAggreagte: QuizWebhookAggregateService,
  ) {}
  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  quizCreated(@Body() quizPayload: QuizWebhookDto) {
    return this.quizWebhookAggreagte.quizCreated(quizPayload);
  }

  @Post('webhook/v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  quizUpdated(@Body() quizPayload: QuizWebhookDto) {
    return this.quizWebhookAggreagte.quizUpdated(quizPayload);
  }

  @Post('webhook/v1/delete')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  quizDeleted(@Body() quizPayload: QuizWebhookDto) {
    return this.quizWebhookAggreagte.quizDeleted(quizPayload);
  }
}
