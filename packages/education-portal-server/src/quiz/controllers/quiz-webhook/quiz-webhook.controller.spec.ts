import { Test, TestingModule } from '@nestjs/testing';
import { QuizWebhookController } from './quiz-webhook.controller';
import { QuizWebhookAggregateService } from '../../../quiz/aggregates/quiz-webhook-aggregate/quiz-webhook-aggregate.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

describe('QuizWebhook Controller', () => {
  let controller: QuizWebhookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: QuizWebhookAggregateService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
      ],
      controllers: [QuizWebhookController],
    }).compile();

    controller = module.get<QuizWebhookController>(QuizWebhookController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
