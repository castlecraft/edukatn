import { Test, TestingModule } from '@nestjs/testing';
import { QuizPoliciesService } from './quiz-policies.service';

describe('QuizPoliciesService', () => {
  let service: QuizPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [QuizPoliciesService],
    }).compile();

    service = module.get<QuizPoliciesService>(QuizPoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
