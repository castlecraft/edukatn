import { QuizAddedCommandHandler } from './quiz-added/quiz-added.handler';
import { QuizRemovedCommandHandler } from './quiz-removed/quiz.removed.handler';
import { QuizUpdatedCommandHandler } from './quiz-updated/quiz-updated.handler';

export const QuizEventManager = [
  QuizAddedCommandHandler,
  QuizRemovedCommandHandler,
  QuizUpdatedCommandHandler,
];
