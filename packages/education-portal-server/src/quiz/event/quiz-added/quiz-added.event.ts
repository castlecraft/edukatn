import { IEvent } from '@nestjs/cqrs';
import { Quiz } from '../../entity/quiz/quiz.entity';

export class QuizAddedEvent implements IEvent {
  constructor(public quiz: Quiz, public clientHttpRequest: any) {}
}
