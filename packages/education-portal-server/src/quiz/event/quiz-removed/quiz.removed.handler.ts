import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { QuizService } from '../../entity/quiz/quiz.service';
import { QuizRemovedEvent } from './quiz-removed.event';

@EventsHandler(QuizRemovedEvent)
export class QuizRemovedCommandHandler
  implements IEventHandler<QuizRemovedEvent> {
  constructor(private readonly quizService: QuizService) {}
  async handle(event: QuizRemovedEvent) {
    const { quiz } = event;
    await this.quizService.deleteOne({ uuid: quiz.uuid });
  }
}
