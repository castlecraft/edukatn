import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { QuizDto } from '../../entity/quiz/quiz-dto';
import { Quiz } from '../../entity/quiz/quiz.entity';
import { QuizAddedEvent } from '../../event/quiz-added/quiz-added.event';
import { QuizService } from '../../entity/quiz/quiz.service';
import { QuizRemovedEvent } from '../../event/quiz-removed/quiz-removed.event';
import { QuizUpdatedEvent } from '../../event/quiz-updated/quiz-updated.event';
import { UpdateQuizDto } from '../../entity/quiz/update-quiz-dto';
import { TopicService } from '../../../topic/entity/topic/topic.service';
import { from, throwError, of } from 'rxjs';
import { switchMap, mergeMap, toArray } from 'rxjs/operators';
import { QuestionService } from '../../../question/entity/question/question.service';
import {
  TOPIC_NOT_FOUND,
  QUIZ_NOT_FOUND,
  QUESTIONS_NOT_FOUND,
  COURSE_NOT_FOUND,
} from '../../../constants/messages';
import { TopicContentDto } from '../../../topic/entity/topic/topic.entity';
import { TeacherService } from '../../../teacher/entity/teacher/teacher.service';
import { CourseService } from '../../../course/entity/course/course.service';
import { UpdateTopicStatusDto } from '../../../topic/entity/topic/update-topic-status-dto';

@Injectable()
export class QuizAggregateService extends AggregateRoot {
  constructor(
    private readonly quizService: QuizService,
    private readonly topicService: TopicService,
    private readonly courseService: CourseService,
    private readonly teacherService: TeacherService,
    private readonly questionService: QuestionService,
  ) {
    super();
  }

  addQuiz(quizPayload: QuizDto, clientHttpRequest) {
    const quiz = new Quiz();
    Object.assign(quiz, quizPayload);
    quiz.uuid = uuidv4();
    this.apply(new QuizAddedEvent(quiz, clientHttpRequest));
  }

  retrieveQuiz(
    offset,
    limit,
    sort,
    search,
    getQuizPayload: UpdateTopicStatusDto,
    clientHttpRequest,
  ) {
    return from(
      this.teacherService.findOne({
        instructor_email: clientHttpRequest.token.email,
        'instructor_log.program': decodeURIComponent(
          getQuizPayload.program_name,
        ),
        'instructor_log.course': decodeURIComponent(getQuizPayload.course_name),
      }),
    ).pipe(
      switchMap(teacher => {
        if (!teacher) {
          return throwError(new NotFoundException());
        }
        return from(
          this.courseService.findOne({
            course_name: decodeURIComponent(getQuizPayload.course_name),
            'topics.topic_name': decodeURIComponent(getQuizPayload.topic_name),
          }),
        ).pipe(
          switchMap(course => {
            if (!course) {
              return throwError(new NotFoundException(COURSE_NOT_FOUND));
            }
            return from(
              this.topicService.findOne({
                name: decodeURIComponent(getQuizPayload.topic_name),
              }),
            ).pipe(
              switchMap(topic => {
                if (!topic) {
                  return throwError(new NotFoundException(TOPIC_NOT_FOUND));
                }
                return this.getQuizName(topic.topic_content).pipe(
                  switchMap(quizname => {
                    return this.getQuestionLinks(quizname).pipe(
                      switchMap((questionLinks: Array<string>) => {
                        return from(
                          this.questionService.list(
                            offset,
                            limit,
                            search,
                            sort,
                            questionLinks,
                          ),
                        ).pipe(
                          switchMap(questionList => {
                            if (!questionList) {
                              return throwError(
                                new NotFoundException(QUESTIONS_NOT_FOUND),
                              );
                            }
                            return of(questionList);
                          }),
                        );
                      }),
                    );
                  }),
                );
              }),
            );
          }),
        );
      }),
    );
  }

  getQuizName(topic_content: TopicContentDto[]) {
    let quizName = '';
    return from(topic_content).pipe(
      mergeMap(topic => {
        quizName = topic.content_type === 'Quiz' ? topic.content : quizName;
        return of(quizName);
      }),
    );
  }

  getQuestionLinks(quizName: string) {
    return from(this.quizService.findOne({ name: quizName })).pipe(
      switchMap(quiz => {
        if (!quiz) {
          return throwError(new NotFoundException(QUIZ_NOT_FOUND));
        }
        return from(quiz.question).pipe(
          mergeMap(eachQuestion => {
            return of(eachQuestion.question_link);
          }),
        );
      }),
      toArray(),
    );
  }

  async getQuizList(offset, limit, sort, search, clientHttpRequest) {
    return await this.quizService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.quizService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new QuizRemovedEvent(found));
  }

  async update(updatePayload: UpdateQuizDto) {
    const provider = await this.quizService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new QuizUpdatedEvent(update));
  }
}
