import { Test, TestingModule } from '@nestjs/testing';
import { QuizWebhookAggregateService } from './quiz-webhook-aggregate.service';
import { QuizService } from '../../../quiz/entity/quiz/quiz.service';

describe('QuizWebhookAggregateService', () => {
  let service: QuizWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        QuizWebhookAggregateService,
        {
          provide: QuizService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<QuizWebhookAggregateService>(
      QuizWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
