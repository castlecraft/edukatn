import { AddQuizCommandHandler } from './add-quiz/add-quiz.handler';
import { RemoveQuizCommandHandler } from './remove-quiz/remove-quiz.handler';
import { UpdateQuizCommandHandler } from './update-quiz/update-quiz.handler';

export const QuizCommandManager = [
  AddQuizCommandHandler,
  RemoveQuizCommandHandler,
  UpdateQuizCommandHandler,
];
