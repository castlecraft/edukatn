import { Module, HttpModule } from '@nestjs/common';
import { QuestionAggregatesManager } from './aggregates';
import { QuestionEntitiesModule } from './entity/entity.module';
import { QuestionQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { QuestionCommandManager } from './command';
import { QuestionEventManager } from './event';
import { QuestionController } from './controllers/question/question.controller';
import { QuestionPoliciesService } from './policies/question-policies/question-policies.service';
import { QuestionWebhookController } from './controllers/question-webhook/question-webhook.controller';
import { QuizEntitiesModule } from '../quiz/entity/entity.module';

@Module({
  imports: [QuestionEntitiesModule, CqrsModule, HttpModule, QuizEntitiesModule],
  controllers: [QuestionController, QuestionWebhookController],
  providers: [
    ...QuestionAggregatesManager,
    ...QuestionQueryManager,
    ...QuestionEventManager,
    ...QuestionCommandManager,
    QuestionPoliciesService,
  ],
  exports: [QuestionEntitiesModule],
})
export class QuestionModule {}
