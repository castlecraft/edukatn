import { Test, TestingModule } from '@nestjs/testing';
import { QuestionWebhookAggregateService } from './question-webhook-aggregate.service';
import { QuestionService } from '../../entity/question/question.service';

describe('QuestionWebhookAggregateService', () => {
  let service: QuestionWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        QuestionWebhookAggregateService,
        {
          provide: QuestionService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<QuestionWebhookAggregateService>(
      QuestionWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
