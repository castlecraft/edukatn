import {
  Injectable,
  NotFoundException,
  NotImplementedException,
  HttpService,
  BadRequestException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { QuestionDto } from '../../entity/question/question-dto';
import { Question } from '../../entity/question/question.entity';
import { QuestionAddedEvent } from '../../event/question-added/question-added.event';
import { QuestionService } from '../../entity/question/question.service';
import { QuestionUpdatedEvent } from '../../event/question-updated/question-updated.event';
import { UpdateQuestionDto } from '../../entity/question/update-question-dto';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { switchMap, map, catchError } from 'rxjs/operators';
import { throwError, of } from 'rxjs';
import {
  FRAPPE_API_GET_QUESTION_ENDPOINT,
  FRAPPE_API_GET_QUIZ_ENDPOINT,
  FRAPPE_API_GET_TOPIC_ENDPOINT,
} from '../../../constants/routes';
import {
  AUTHORIZATION,
  BEARER_HEADER_VALUE_PREFIX,
  APPLICATION_JSON_CONTENT_TYPE,
  ACCEPT,
  CONTENT_TYPE,
  FORWARD_SLASH,
} from '../../../constants/app-strings';
import { ServerSettings } from '../../../system-settings/entities/server-settings/server-settings.entity';
import { QuizDetails } from '../../entity/question/question-interface';
import { QuizWebhookAggregateService } from '../../../quiz/aggregates/quiz-webhook-aggregate/quiz-webhook-aggregate.service';
@Injectable()
export class QuestionAggregateService extends AggregateRoot {
  constructor(
    private readonly questionService: QuestionService,
    private readonly quizWebhookService: QuizWebhookAggregateService,
    private readonly settings: SettingsService,
    private readonly http: HttpService,
  ) {
    super();
  }

  addQuestion(questionPayload: QuestionDto, clientHttpRequest) {
    const question = new Question();
    Object.assign(question, questionPayload);
    question.uuid = uuidv4();
    this.apply(new QuestionAddedEvent(question, clientHttpRequest));
  }

  async retrieveQuestion(uuid: string, req) {
    const provider = await this.questionService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getQuestionList(offset, limit, sort, search, clientHttpRequest) {
    return await this.questionService.list(offset, limit, search, sort);
  }

  removeQuestion(removeQuestionPayload: UpdateQuestionDto, clientHttpRequest) {
    return this.settings.find().pipe(
      switchMap(settings => {
        if (!settings) {
          return throwError(new NotImplementedException());
        }
        const url = `${settings.authServerURL}${FRAPPE_API_GET_QUIZ_ENDPOINT}${FORWARD_SLASH}${removeQuestionPayload.quiz_name}`;
        const headers = {
          headers: this.settings.getAuthorizationHeaders(
            clientHttpRequest.token,
          ),
        };
        return this.http.get(url, headers).pipe(
          map(res => res.data.data),
          switchMap(res => {
            res.question = res.question.filter(question => {
              if (question.question_link !== removeQuestionPayload.name) {
                return question;
              }
            });
            const body = {
              question: res.question,
            };
            return this.http.put(url, JSON.stringify(body), headers);
          }),
          map(res => res.data.data),
          switchMap(res => {
            return this.quizWebhookService.quizUpdated(res);
          }),
          catchError(err => {
            if (
              err.response &&
              err.response.data &&
              err.response.status === 404
            ) {
              return throwError(
                new BadRequestException(
                  this.settings.getErrorMessage(err.response.data),
                ),
              );
            } else {
              return throwError(new BadRequestException(err));
            }
          }),
        );
      }),
    );
  }

  updateQuestion(updatePayload: UpdateQuestionDto, clientHttpRequest) {
    return this.settings.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(new NotImplementedException());
        }
        const url = `${settings.authServerURL}${FRAPPE_API_GET_QUESTION_ENDPOINT}/${updatePayload.name}`;
        const headers = {
          headers: this.settings.getAuthorizationHeaders(
            clientHttpRequest.token,
          ),
        };
        const body = {
          question: updatePayload.question,
          options: updatePayload.options,
        };
        return this.http.get(url, headers).pipe(
          map(res => res.data.data),
          switchMap(res => {
            return this.http.put(url, JSON.stringify(body), headers);
          }),
          catchError(err => {
            if (err.response && err.response.data) {
              return throwError(
                new BadRequestException(
                  this.settings.getErrorMessage(err.response.data),
                ),
              );
            } else {
              return throwError(new BadRequestException(err));
            }
          }),
        );
      }),
      map(res => res.data.data),
      switchMap(res => {
        this.apply(new QuestionUpdatedEvent(res));
        return of({});
      }),
    );
  }

  createQuestion(questionPayload: QuestionDto, clientHttpRequest) {
    return this.settings.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(new NotImplementedException());
        }
        const url = `${settings.authServerURL}${FRAPPE_API_GET_QUESTION_ENDPOINT}`;
        const headers = this.getHeaders(clientHttpRequest.token.accessToken);

        return this.http
          .post(url, JSON.stringify(questionPayload), { headers })
          .pipe(
            map(res => res.data.data),
            switchMap((question: { name: string }) => {
              return this.checkQuiz(
                questionPayload,
                question.name,
                settings,
                clientHttpRequest.token.accessToken,
              );
            }),
          );
      }),
    );
  }

  checkQuiz(
    questionPayload: QuestionDto,
    question_name: string,
    settings: ServerSettings,
    token,
  ) {
    const url = `${settings.authServerURL}${FRAPPE_API_GET_QUIZ_ENDPOINT}/${questionPayload.topic_name}`;
    const headers = this.getHeaders(token);
    return this.http.get(url, { headers }).pipe(
      map(res => res.data.data),
      switchMap((quiz: QuizDetails) => {
        return this.updateQuiz(quiz, question_name, settings, token);
      }),
      catchError(err => {
        return this.createQuiz(
          questionPayload.topic_name,
          question_name,
          settings,
          token,
        );
      }),
    );
  }

  updateQuiz(
    quiz: QuizDetails,
    question_name: string,
    settings: ServerSettings,
    token,
  ) {
    const url = `${settings.authServerURL}${FRAPPE_API_GET_QUIZ_ENDPOINT}/${quiz.name}`;
    quiz.question.push({ question_link: question_name });
    const headers = this.getHeaders(token);

    return this.http
      .put(url, JSON.stringify(quiz), { headers })
      .pipe(map(res => res.data.data));
  }

  createQuiz(
    topic_name: string,
    question_name: string,
    settings: ServerSettings,
    token,
  ) {
    const url = `${settings.authServerURL}${FRAPPE_API_GET_QUIZ_ENDPOINT}`;
    const body = this.getCreateQuizBody(topic_name, question_name);
    const headers = this.getHeaders(token);

    return this.http.post(url, JSON.stringify(body), { headers }).pipe(
      map(res => res.data.data),
      switchMap((quiz: { name: string }) => {
        return this.getTopic(quiz.name, settings, token);
      }),
    );
  }

  getTopic(topic_name: string, settings: ServerSettings, token) {
    const url = `${settings.authServerURL}${FRAPPE_API_GET_TOPIC_ENDPOINT}/${topic_name}`;
    const headers = this.getHeaders(token);

    return this.http.get(url, { headers }).pipe(
      map(res => res.data.data),
      switchMap((topic: { name: string; topic_content: any[] }) => {
        topic.topic_content.push({
          content_type: 'Quiz',
          content: topic.name,
        });
        return this.updateTopic(topic, settings, token);
      }),
    );
  }

  updateTopic(topic: any, settings: ServerSettings, token) {
    const url = `${settings.authServerURL}${FRAPPE_API_GET_TOPIC_ENDPOINT}/${topic.name}`;
    const headers = this.getHeaders(token);

    return this.http
      .put(url, JSON.stringify(topic), { headers })
      .pipe(map(res => res.data.data));
  }

  getCreateQuizBody(topic_name: string, question_name: string) {
    const quiz: any = {};
    quiz.title = topic_name;
    quiz.question = [];
    quiz.question.push({ question_link: question_name });
    return quiz;
  }

  getHeaders(accessToken) {
    const headers: any = {};
    headers[AUTHORIZATION] = BEARER_HEADER_VALUE_PREFIX + accessToken;
    headers[CONTENT_TYPE] = APPLICATION_JSON_CONTENT_TYPE;
    headers[ACCEPT] = APPLICATION_JSON_CONTENT_TYPE;
    return headers;
  }
}
