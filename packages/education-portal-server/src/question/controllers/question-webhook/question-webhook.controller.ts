import {
  Controller,
  Post,
  Body,
  UsePipes,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { QuestionWebhookDto } from '../../entity/question/question-webhook-dto';
import { QuestionWebhookAggregateService } from '../../aggregates/question-webhook-aggregate/question-webhook-aggregate.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';

@Controller('question')
export class QuestionWebhookController {
  constructor(
    private readonly questionWebhookAggreagte: QuestionWebhookAggregateService,
  ) {}
  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  questionCreated(@Body() questionPayload: QuestionWebhookDto) {
    return this.questionWebhookAggreagte.questionCreated(questionPayload);
  }

  @Post('webhook/v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  questionUpdated(@Body() questionPayload: QuestionWebhookDto) {
    return this.questionWebhookAggreagte.questionUpdated(questionPayload);
  }

  @Post('webhook/v1/delete')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  questionDeleted(@Body() questionPayload: QuestionWebhookDto) {
    return this.questionWebhookAggreagte.questionDeleted(questionPayload);
  }
}
