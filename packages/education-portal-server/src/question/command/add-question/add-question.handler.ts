import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddQuestionCommand } from './add-question.command';
import { QuestionAggregateService } from '../../aggregates/question-aggregate/question-aggregate.service';

@CommandHandler(AddQuestionCommand)
export class AddQuestionCommandHandler
  implements ICommandHandler<AddQuestionCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: QuestionAggregateService,
  ) {}
  async execute(command: AddQuestionCommand) {
    const { questionPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addQuestion(questionPayload, clientHttpRequest);
    aggregate.commit();
  }
}
