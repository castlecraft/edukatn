export interface QuizDetails {
  name: string;
  question: QuestionDetails[];
}

export interface QuestionDetails {
  question_link: string;
}
