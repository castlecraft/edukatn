import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { QuestionUpdatedEvent } from './question-updated.event';
import { QuestionService } from '../../entity/question/question.service';

@EventsHandler(QuestionUpdatedEvent)
export class QuestionUpdatedCommandHandler
  implements IEventHandler<QuestionUpdatedEvent> {
  constructor(private readonly object: QuestionService) {}

  async handle(event: QuestionUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { name: updatePayload.name },
      { $set: updatePayload },
    );
  }
}
