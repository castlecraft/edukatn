import { QuestionAddedCommandHandler } from './question-added/question-added.handler';
import { QuestionRemovedCommandHandler } from './question-removed/question.removed.handler';
import { QuestionUpdatedCommandHandler } from './question-updated/question-updated.handler';

export const QuestionEventManager = [
  QuestionAddedCommandHandler,
  QuestionRemovedCommandHandler,
  QuestionUpdatedCommandHandler,
];
