import {
  Injectable,
  BadRequestException,
  NotImplementedException,
  InternalServerErrorException,
  HttpService,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { Observable, from, forkJoin, throwError } from 'rxjs';
import { ServerSettingsService } from '../../entities/server-settings/server-settings.service';
import { ServerSettings } from '../../entities/server-settings/server-settings.entity';
import {
  BEARER_HEADER_VALUE_PREFIX,
  APPLICATION_JSON_CONTENT_TYPE,
  AUTHORIZATION,
  CONTENT_TYPE_HEADER_KEY,
  ACCEPT,
} from '../../../constants/app-strings';
import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';
import {
  PLEASE_RUN_SETUP,
  SETUP_ALREADY_COMPLETE,
} from '../../../constants/messages';
import { randomBytes } from 'crypto';
import { CONTENT_TYPE } from '../../../constants/app-strings';
import { switchMap, catchError, map } from 'rxjs/operators';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import {
  programAfterInsertWebhookData,
  programOnUpdateWebhookData,
  programOnTrashWebhookData,
  courseOnUpdateWebhookData,
  courseAfterInsertWebhookData,
  courseOnTrashWebhookData,
  articleOnUpdateWebhookData,
  articleOnTrashWebhookData,
  articleAfterInsertWebhookData,
  quizAfterInsertWebhookData,
  quizOnTrashWebhookData,
  quizOnUpdateWebhookData,
  questionAfterInsertWebhookData,
  questionOnTrashWebhookData,
  questionOnUpdateWebhookData,
  departmentAfterInsertWebhookData,
  departmentOnTrashWebhookData,
  departmentOnUpdateWebhookData,
  studentAfterInsertWebhookData,
  studentOnUpdateWebhookData,
  studentOnTrashWebhookData,
  teacherOnTrashWebhookData,
  teacherOnUpdateWebhookData,
  teacherAfterInsertWebhookData,
  topicAfterInsertWebhookData,
  topicOnTrashWebhookData,
  topicOnUpdateWebhookData,
  videoAfterInsertWebhookData,
  videoOnTrashWebhookData,
  videoOnUpdateWebhookData,
  attendanceAfterInsertWebhookData,
  attendanceOnTrashWebhookData,
  attendanceOnUpdateWebhookData,
  studentGroupAfterInsertWebhookData,
  studentGroupOnUpdateWebhookData,
  studentGroupOnTrashWebhookData,
  roomAfterInsertWebhookData,
  roomOnTrashWebhookData,
  roomOnUpdateWebhookData,
  courseScheduleOnUpdateWebhookData,
  courseScheduleOnTrashWebhookData,
  courseScheduleAfterInsertWebhookData,
} from '../../../constants/webhook-data';
import {
  PROGRAM_AFTER_INSERT_ENDPOINT,
  PROGRAM_ON_UPDATE_ENDPOINT,
  PROGRAM_ON_TRASH_ENDPOINT,
  COURSE_ON_TRASH_ENDPOINT,
  COURSE_ON_AFTER_INSERT_ENDPOINT,
  COURSE_ON_UPDATE_ENDPOINT,
  ARTICLE_ON_AFTER_INSERT_ENDPOINT,
  ARTICLE_ON_TRASH_ENDPOINT,
  ARTICLE_ON_UPDATE_ENDPOINT,
  QUIZ_ON_UPDATE_ENDPOINT,
  QUIZ_ON_TRASH_ENDPOINT,
  QUIZ_ON_AFTER_INSERT_ENDPOINT,
  QUESTION_ON_AFTER_INSERT_ENDPOINT,
  QUESTION_ON_TRASH_ENDPOINT,
  QUESTION_ON_UPDATE_ENDPOINT,
  DEPARTMENT_ON_AFTER_INSERT_ENDPOINT,
  DEPARTMENT_ON_TRASH_ENDPOINT,
  DEPARTMENT_ON_UPDATE_ENDPOINT,
  STUDENT_AFTER_INSERT_ENDPOINT,
  STUDENT_ON_UPDATE_ENDPOINT,
  STUDENT_ON_TRASH_ENDPOINT,
  TEACHER_AFTER_INSERT_ENDPOINT,
  TEACHER_ON_UPDATE_ENDPOINT,
  TEACHER_ON_TRASH_ENDPOINT,
  TOPIC_ON_AFTER_INSERT_ENDPOINT,
  TOPIC_ON_TRASH_ENDPOINT,
  TOPIC_ON_UPDATE_ENDPOINT,
  VIDEO_ON_AFTER_INSERT_ENDPOINT,
  VIDEO_ON_TRASH_ENDPOINT,
  VIDEO_ON_UPDATE_ENDPOINT,
  ATTENDANCE_ON_AFTER_INSERT_ENDPOINT,
  ATTENDANCE_ON_TRASH_ENDPOINT,
  ATTENDANCE_ON_UPDATE_ENDPOINT,
  STUDENT_GROUP_ON_AFTER_INSERT_ENDPOINT,
  STUDENT_GROUP_ON_TRASH_ENDPOINT,
  STUDENT_GROUP_ON_UPDATE_ENDPOINT,
  ROOM_ON_AFTER_INSERT_ENDPOINT,
  ROOM_ON_TRASH_ENDPOINT,
  ROOM_ON_UPDATE_ENDPOINT,
  COURSE_SCHEDULE_ON_AFTER_INSERT_ENDPOINT,
  COURSE_SCHEDULE_ON_TRASH_ENDPOINT,
  COURSE_SCHEDULE_ON_UPDATE_ENDPOINT,
} from '../../../constants/routes';

@Injectable()
export class SettingsService extends AggregateRoot {
  constructor(
    private readonly serverSettingsService: ServerSettingsService,
    private readonly http: HttpService,
    private readonly clientToken: ClientTokenManagerService,
  ) {
    super();
  }

  find(): Observable<ServerSettings> {
    const settings = this.serverSettingsService.find();
    return from(settings);
  }

  update(query, params) {
    return from(this.serverSettingsService.update(query, params));
  }

  getHeaders(token: TokenCache) {
    const headers = {};
    headers[
      AUTHORIZATION
    ] = `${BEARER_HEADER_VALUE_PREFIX} ${token.accessToken}`;
    headers[CONTENT_TYPE_HEADER_KEY] = APPLICATION_JSON_CONTENT_TYPE;
    return headers;
  }

  getAuthorizationHeaders(token: TokenCache) {
    const headers: any = {};
    headers[AUTHORIZATION] = BEARER_HEADER_VALUE_PREFIX + token.accessToken;
    headers[CONTENT_TYPE] = APPLICATION_JSON_CONTENT_TYPE;
    headers[ACCEPT] = APPLICATION_JSON_CONTENT_TYPE;
    return headers;
  }

  getErrorMessage(data: any) {
    const err = data.exc
      ? this.getTracebackError(data.exc)
      : data._server_messages
      ? this.getServerMessageError(data._server_messages)
      : data;
    return err;
  }

  getServerMessageError(msg: string) {
    let err = JSON.parse(msg)[0];
    err = JSON.parse(err).message;
    return err;
  }

  getTracebackError(msg: string) {
    const err = JSON.parse(msg)[0]
      .split('\n')
      .slice(-2)[0];
    return err;
  }

  async setupFrappeWebhookKey() {
    const settings = await this.serverSettingsService.find();
    if (!settings) throw new NotImplementedException(PLEASE_RUN_SETUP);
    if (settings.webhookApiKey)
      throw new BadRequestException(SETUP_ALREADY_COMPLETE);
    settings.webhookApiKey = this.randomBytes32();
    settings.save();
    return settings;
  }

  async updateFrappeWebhookKey() {
    const settings = await this.serverSettingsService.find();
    if (!settings) throw new NotImplementedException(PLEASE_RUN_SETUP);
    settings.webhookApiKey = this.randomBytes32();
    settings.save();
    return settings;
  }

  randomBytes32() {
    return randomBytes(32).toString('hex');
  }

  setupWebhooks() {
    let serverSettings: ServerSettings;
    const headers = {};

    headers[CONTENT_TYPE] = APPLICATION_JSON_CONTENT_TYPE;
    headers[ACCEPT] = APPLICATION_JSON_CONTENT_TYPE;

    return this.find().pipe(
      switchMap(settings => {
        serverSettings = settings;
        return this.clientToken.getClientToken();
      }),
      switchMap(token => {
        headers[AUTHORIZATION] = BEARER_HEADER_VALUE_PREFIX + token.accessToken;
        return forkJoin(
          // Student Webhooks
          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              studentAfterInsertWebhookData(
                serverSettings.appURL + STUDENT_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              studentOnUpdateWebhookData(
                serverSettings.appURL + STUDENT_ON_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              studentOnTrashWebhookData(
                serverSettings.appURL + STUDENT_ON_TRASH_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          // Teacher Webhooks
          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              teacherAfterInsertWebhookData(
                serverSettings.appURL + TEACHER_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              teacherOnUpdateWebhookData(
                serverSettings.appURL + TEACHER_ON_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              teacherOnTrashWebhookData(
                serverSettings.appURL + TEACHER_ON_TRASH_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          // Program Webhooks
          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              programAfterInsertWebhookData(
                serverSettings.appURL + PROGRAM_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              programOnUpdateWebhookData(
                serverSettings.appURL + PROGRAM_ON_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              programOnTrashWebhookData(
                serverSettings.appURL + PROGRAM_ON_TRASH_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          // COURSE WEBHOOKS

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              courseAfterInsertWebhookData(
                serverSettings.appURL + COURSE_ON_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              courseOnTrashWebhookData(
                serverSettings.appURL + COURSE_ON_TRASH_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              courseOnUpdateWebhookData(
                serverSettings.appURL + COURSE_ON_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          // Topics WEBHOOKS

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              topicAfterInsertWebhookData(
                serverSettings.appURL + TOPIC_ON_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              topicOnTrashWebhookData(
                serverSettings.appURL + TOPIC_ON_TRASH_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              topicOnUpdateWebhookData(
                serverSettings.appURL + TOPIC_ON_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          // Video WEBHOOKS

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              videoAfterInsertWebhookData(
                serverSettings.appURL + VIDEO_ON_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              videoOnTrashWebhookData(
                serverSettings.appURL + VIDEO_ON_TRASH_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              videoOnUpdateWebhookData(
                serverSettings.appURL + VIDEO_ON_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          // article webhooks
          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              articleAfterInsertWebhookData(
                serverSettings.appURL + ARTICLE_ON_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              articleOnTrashWebhookData(
                serverSettings.appURL + ARTICLE_ON_TRASH_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              articleOnUpdateWebhookData(
                serverSettings.appURL + ARTICLE_ON_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          // quiz webhooks
          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              quizAfterInsertWebhookData(
                serverSettings.appURL + QUIZ_ON_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              quizOnTrashWebhookData(
                serverSettings.appURL + QUIZ_ON_TRASH_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              quizOnUpdateWebhookData(
                serverSettings.appURL + QUIZ_ON_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          // question webhooks
          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              questionAfterInsertWebhookData(
                serverSettings.appURL + QUESTION_ON_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              questionOnTrashWebhookData(
                serverSettings.appURL + QUESTION_ON_TRASH_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              questionOnUpdateWebhookData(
                serverSettings.appURL + QUESTION_ON_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          // department webhooks

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              departmentAfterInsertWebhookData(
                serverSettings.appURL + DEPARTMENT_ON_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              departmentOnTrashWebhookData(
                serverSettings.appURL + DEPARTMENT_ON_TRASH_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              departmentOnUpdateWebhookData(
                serverSettings.appURL + DEPARTMENT_ON_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          // Student Attendance webhooks

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              attendanceAfterInsertWebhookData(
                serverSettings.appURL + ATTENDANCE_ON_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              attendanceOnTrashWebhookData(
                serverSettings.appURL + ATTENDANCE_ON_TRASH_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              attendanceOnUpdateWebhookData(
                serverSettings.appURL + ATTENDANCE_ON_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          // Student Group webhooks

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              studentGroupAfterInsertWebhookData(
                serverSettings.appURL + STUDENT_GROUP_ON_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              studentGroupOnTrashWebhookData(
                serverSettings.appURL + STUDENT_GROUP_ON_TRASH_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              studentGroupOnUpdateWebhookData(
                serverSettings.appURL + STUDENT_GROUP_ON_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          // Room webhooks

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              roomAfterInsertWebhookData(
                serverSettings.appURL + ROOM_ON_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              roomOnTrashWebhookData(
                serverSettings.appURL + ROOM_ON_TRASH_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              roomOnUpdateWebhookData(
                serverSettings.appURL + ROOM_ON_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          // Course Schedule webhooks

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              courseScheduleAfterInsertWebhookData(
                serverSettings.appURL +
                  COURSE_SCHEDULE_ON_AFTER_INSERT_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              courseScheduleOnTrashWebhookData(
                serverSettings.appURL + COURSE_SCHEDULE_ON_TRASH_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),

          this.http
            .post(
              serverSettings.authServerURL + '/api/resource/Webhook',
              courseScheduleOnUpdateWebhookData(
                serverSettings.appURL + COURSE_SCHEDULE_ON_UPDATE_ENDPOINT,
                serverSettings.webhookApiKey,
              ),
              { headers },
            )
            .pipe(map(res => res.data)),
        );
      }),
      catchError(error => {
        return throwError(new InternalServerErrorException(error));
      }),
    );
  }
}
