import {
  Controller,
  Post,
  Body,
  UsePipes,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { StudentWebhookDto } from '../../entity/student/student.webhook-dto';
import { StudentWebhookAggregateService } from '../../aggregates/student-webhook-aggregate/student-webhook-aggregate.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';

@Controller('student')
export class StudentWebhookController {
  constructor(
    private readonly studentWebhookAggreagte: StudentWebhookAggregateService,
  ) {}
  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  studentCreated(@Body() studentPayload: StudentWebhookDto) {
    return this.studentWebhookAggreagte.studentCreated(studentPayload);
  }

  @Post('webhook/v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  studentUpdated(@Body() studentPayload: StudentWebhookDto) {
    return this.studentWebhookAggreagte.studentUpdated(studentPayload);
  }

  @Post('webhook/v1/delete')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  studentDeleted(@Body() studentPayload: StudentWebhookDto) {
    return this.studentWebhookAggreagte.studentDeleted(studentPayload);
  }
}
