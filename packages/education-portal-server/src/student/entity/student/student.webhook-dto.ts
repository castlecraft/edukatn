import {
  IsNotEmpty,
  IsString,
  IsNumber,
  IsOptional,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

export class StudentWebhookDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsOptional()
  @IsString()
  owner: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsNumber()
  enabled: number;

  @IsOptional()
  @IsString()
  first_name: string;

  @IsOptional()
  @IsString()
  middle_name: string;

  @IsOptional()
  @IsString()
  last_name: string;

  @IsOptional()
  @IsString()
  naming_series: string;

  @IsOptional()
  @IsString()
  student_email_id: string;

  @IsOptional()
  @IsString()
  student_mobile_number: string;

  @IsOptional()
  @IsString()
  joining_date: string;

  @IsOptional()
  @IsString()
  date_of_birth: string;

  @IsOptional()
  @IsString()
  blood_group: string;

  @IsOptional()
  @IsString()
  gender: string;

  @IsOptional()
  @IsString()
  nationality: string;

  @IsOptional()
  @IsString()
  address_line_1: string;

  @IsOptional()
  @IsString()
  title: string;

  @ValidateNested()
  @Type(() => GaurdiansWebhookDto)
  guardians: GaurdiansWebhookDto[];
}
export class GaurdiansWebhookDto {
  @IsOptional()
  @IsString()
  guardian: string;

  @IsOptional()
  @IsString()
  guardian_name: string;

  @IsOptional()
  @IsString()
  relation: string;
}
