import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Student } from './student/student.entity';
import { StudentService } from './student/student.service';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [TypeOrmModule.forFeature([Student]), CqrsModule],
  providers: [StudentService],
  exports: [StudentService],
})
export class StudentEntitiesModule {}
