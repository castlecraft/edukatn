import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { StudentService } from '../../entity/student/student.service';
import { StudentRemovedEvent } from './student-removed.event';

@EventsHandler(StudentRemovedEvent)
export class StudentRemovedEventHandler
  implements IEventHandler<StudentRemovedEvent> {
  constructor(private readonly studentService: StudentService) {}
  async handle(event: StudentRemovedEvent) {
    const { student } = event;
    await this.studentService.deleteOne({ uuid: student.uuid });
  }
}
