import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { StudentAddedEvent } from './student-added.event';
import { StudentService } from '../../entity/student/student.service';

@EventsHandler(StudentAddedEvent)
export class StudentAddedEventHandler
  implements IEventHandler<StudentAddedEvent> {
  constructor(private readonly studentService: StudentService) {}
  async handle(event: StudentAddedEvent) {
    const { student } = event;
    await this.studentService.create(student);
  }
}
