import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { StudentUpdatedEvent } from './student-updated.event';
import { StudentService } from '../../entity/student/student.service';

@EventsHandler(StudentUpdatedEvent)
export class StudentUpdatedEventHandler
  implements IEventHandler<StudentUpdatedEvent> {
  constructor(private readonly studentService: StudentService) {}

  async handle(event: StudentUpdatedEvent) {
    const { updatePayload } = event;
    await this.studentService.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}
