import { Test, TestingModule } from '@nestjs/testing';
import { StudentWebhookAggregateService } from './student-webhook-aggregate.service';
import { StudentService } from '../../../student/entity/student/student.service';

describe('StudentWebhookAggregateService', () => {
  let service: StudentWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StudentWebhookAggregateService,
        {
          provide: StudentService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<StudentWebhookAggregateService>(
      StudentWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
