import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { StudentDto } from '../../entity/student/student-dto';
import { Student } from '../../entity/student/student.entity';
import { StudentAddedEvent } from '../../event/student-added/student-added.event';
import { StudentService } from '../../entity/student/student.service';
import { StudentRemovedEvent } from '../../event/student-removed/student-removed.event';
import { StudentUpdatedEvent } from '../../event/student-updated/student-updated.event';
import { UpdateStudentDto } from '../../entity/student/update-student-dto';
import { ClientHttpRequestTokenInterface } from '../../../common/client-request-token.interace';

@Injectable()
export class StudentAggregateService extends AggregateRoot {
  constructor(private readonly studentService: StudentService) {
    super();
  }

  addStudent(
    studentPayload: StudentDto,
    clientHttpRequest: ClientHttpRequestTokenInterface,
  ) {
    const student = new Student();
    Object.assign(student, studentPayload);
    student.uuid = uuidv4();
    this.apply(new StudentAddedEvent(student, clientHttpRequest));
  }

  async retrieveStudent(uuid: string, req: ClientHttpRequestTokenInterface) {
    const provider = await this.studentService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getStudentList(
    offset,
    limit,
    sort,
    search,
    clientHttpRequest: ClientHttpRequestTokenInterface,
  ) {
    return await this.studentService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.studentService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new StudentRemovedEvent(found));
  }

  async update(updatePayload: UpdateStudentDto) {
    const provider = await this.studentService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new StudentUpdatedEvent(update));
  }
}
