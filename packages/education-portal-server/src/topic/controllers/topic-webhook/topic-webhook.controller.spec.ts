import { Test, TestingModule } from '@nestjs/testing';
import { TopicWebhookController } from './topic-webhook.controller';
import { TopicWebhookAggregateService } from '../../aggregates/topic-webhook-aggregate/topic-webhook-aggregate.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

describe('TopicWebhook Controller', () => {
  let controller: TopicWebhookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: TopicWebhookAggregateService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
      ],
      controllers: [TopicWebhookController],
    }).compile();

    controller = module.get<TopicWebhookController>(TopicWebhookController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
