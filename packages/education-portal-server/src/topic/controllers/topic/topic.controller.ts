import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { TopicDto } from '../../entity/topic/topic-dto';
import { AddTopicCommand } from '../../command/add-topic/add-topic.command';
import { RemoveTopicCommand } from '../../command/remove-topic/remove-topic.command';
import { UpdateTopicCommand } from '../../command/update-topic/update-topic.command';
import { RetrieveTopicQuery } from '../../query/get-topic/retrieve-topic.query';
import { RetrieveTopicListQuery } from '../../query/list-topic/retrieve-topic-list.query';
import { UpdateTopicDto } from '../../entity/topic/update-topic-dto';
import { AddExtraTopicCommand } from '../../../topic/command/add-extra-topic/add-extra-topic.command';
import { UpdateTopicStatusCommand } from '../../../topic/command/update-course-topic/update-course-topic.command';
import { UpdateTopicStatusDto } from '../../../topic/entity/topic/update-topic-status-dto';

@Controller('topic')
export class TopicController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() topicPayload: TopicDto, @Req() req) {
    return this.commandBus.execute(new AddTopicCommand(topicPayload, req));
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid: string) {
    return this.commandBus.execute(new RemoveTopicCommand(uuid));
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getClient(@Param('uuid') uuid: string, @Req() req) {
    return await this.queryBus.execute(new RetrieveTopicQuery(uuid, req));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getClientList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Req() clientHttpRequest,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveTopicListQuery(
        offset,
        limit,
        sort,
        search,
        clientHttpRequest,
      ),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateClient(@Body() updatePayload: UpdateTopicDto) {
    return this.commandBus.execute(new UpdateTopicCommand(updatePayload));
  }

  @Post('v1/update_status')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateTopicStatus(
    @Body() updateTopicPayload: UpdateTopicStatusDto,
    @Req() req,
  ) {
    return this.commandBus.execute(
      new UpdateTopicStatusCommand(updateTopicPayload, req),
    );
  }

  @Post('v1/add_topic')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  addExtraTopic(@Body() addTopicPayload: UpdateTopicStatusDto, @Req() req) {
    return this.commandBus.execute(
      new AddExtraTopicCommand(addTopicPayload, req),
    );
  }
}
