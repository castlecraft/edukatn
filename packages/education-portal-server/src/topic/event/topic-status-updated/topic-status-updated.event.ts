import { IEvent } from '@nestjs/cqrs';
import { Course } from '../../../course/entity/course/course.entity';

export class TopicStatusUpdatedEvent implements IEvent {
  constructor(public updateStatusPayload: Course) {}
}
