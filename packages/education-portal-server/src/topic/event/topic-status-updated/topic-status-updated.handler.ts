import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { CourseService } from '../../../course/entity/course/course.service';
import { TopicStatusUpdatedEvent } from './topic-status-updated.event';

@EventsHandler(TopicStatusUpdatedEvent)
export class TopicStatusUpdatedEventHandler
  implements IEventHandler<TopicStatusUpdatedEvent> {
  constructor(private readonly object: CourseService) {}

  async handle(event: TopicStatusUpdatedEvent) {
    const { updateStatusPayload } = event;
    await this.object.updateOne(
      { course_name: updateStatusPayload.course_name },
      { $set: updateStatusPayload },
    );
  }
}
