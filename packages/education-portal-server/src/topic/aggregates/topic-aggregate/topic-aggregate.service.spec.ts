import { Test, TestingModule } from '@nestjs/testing';
import { TopicAggregateService } from './topic-aggregate.service';
import { TopicService } from '../../entity/topic/topic.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { HttpService } from '@nestjs/common';
import { CourseService } from '../../../course/entity/course/course.service';
import { CourseWebhookAggregateService } from '../../../course/aggregates/course-webhook-aggregate/course-webhook-aggregate.service';

describe('TopicAggregateService', () => {
  let service: TopicAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TopicAggregateService,
        {
          provide: TopicService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: CourseWebhookAggregateService,
          useValue: {},
        },
        {
          provide: CourseService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<TopicAggregateService>(TopicAggregateService);
  });
  TopicAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
