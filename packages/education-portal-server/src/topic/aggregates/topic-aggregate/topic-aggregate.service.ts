import {
  Injectable,
  NotFoundException,
  HttpService,
  NotImplementedException,
  BadRequestException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { TopicDto } from '../../entity/topic/topic-dto';
import { Topic } from '../../entity/topic/topic.entity';
import { TopicAddedEvent } from '../../event/topic-added/topic-added.event';
import { TopicService } from '../../entity/topic/topic.service';
import { TopicRemovedEvent } from '../../event/topic-removed/topic-removed.event';
import { TopicUpdatedEvent } from '../../event/topic-updated/topic-updated.event';
import { UpdateTopicDto } from '../../entity/topic/update-topic-dto';
import { throwError } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';
import {
  FRAPPE_API_GET_TOPIC_ENDPOINT,
  FRAPPE_API_GET_COURSE_ENDPOINT,
} from '../../../constants/routes';
import {
  TOPICS_STATUS_ENUM,
  FORWARD_SLASH,
} from '../../../constants/app-strings';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import {
  TOPIC_ALREADY_IN_PROGRESS,
  TOPIC_ALREADY_COMPLETED,
} from '../../../constants/messages';
import { UpdateTopicStatusDto } from '../../../topic/entity/topic/update-topic-status-dto';
import { CourseService } from '../../../course/entity/course/course.service';
import { TopicStatusUpdatedEvent } from '../../event/topic-status-updated/topic-status-updated.event';
import { ServerSettings } from '../../../system-settings/entities/server-settings/server-settings.entity';
import { CourseWebhookAggregateService } from '../../../course/aggregates/course-webhook-aggregate/course-webhook-aggregate.service';

@Injectable()
export class TopicAggregateService extends AggregateRoot {
  constructor(
    private readonly topicService: TopicService,
    private readonly settings: SettingsService,
    private readonly http: HttpService,
    public readonly courseWebhookService: CourseWebhookAggregateService,
    private readonly courseService: CourseService,
  ) {
    super();
  }

  addTopic(topicPayload: TopicDto, clientHttpRequest) {
    const topic = new Topic();
    Object.assign(topic, topicPayload);
    topic.uuid = uuidv4();
    this.apply(new TopicAddedEvent(topic, clientHttpRequest));
  }

  async retrieveTopic(uuid: string, req) {
    const provider = await this.topicService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getTopicList(offset, limit, sort, search, clientHttpRequest) {
    return await this.topicService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.topicService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new TopicRemovedEvent(found));
  }

  async update(updatePayload: UpdateTopicDto) {
    const provider = await this.topicService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new TopicUpdatedEvent(update));
  }

  async updateTopicStatus(updateTopicsPayload: UpdateTopicStatusDto, req) {
    const course = await this.courseService.findOne({
      course_name: updateTopicsPayload.course_name,
    });
    if (!course) {
      throw new NotFoundException();
    }
    const topicMap = {};
    course.topics.forEach(topic => {
      topicMap[topic.topic_name] = topic;
    });
    if (
      topicMap[updateTopicsPayload.topic_name].status ===
      TOPICS_STATUS_ENUM.IN_PROGRESS_STATUS
    ) {
      topicMap[updateTopicsPayload.topic_name].status =
        TOPICS_STATUS_ENUM.COMPLETE_STATUS;
      topicMap[updateTopicsPayload.topic_name].completed_date = new Date();
    } else if (
      topicMap[updateTopicsPayload.topic_name].status ===
      TOPICS_STATUS_ENUM.REMAINING_STATUS
    ) {
      for (const key of Object.keys(topicMap)) {
        if (topicMap[key].status === TOPICS_STATUS_ENUM.IN_PROGRESS_STATUS) {
          throw new BadRequestException(TOPIC_ALREADY_IN_PROGRESS);
        }
      }
      topicMap[updateTopicsPayload.topic_name].status =
        TOPICS_STATUS_ENUM.IN_PROGRESS_STATUS;
    } else {
      throw new BadRequestException(TOPIC_ALREADY_COMPLETED);
    }
    course.topics = [];
    for (const keys of Object.keys(topicMap)) {
      course.topics.push(topicMap[keys]);
    }
    this.apply(new TopicStatusUpdatedEvent(course));
  }

  addExtraTopic(addTopicPayload: UpdateTopicStatusDto, clientHttpRequest) {
    return this.getExistingTopic(addTopicPayload, clientHttpRequest);
  }

  getExistingTopic(addTopicPayload: UpdateTopicStatusDto, clientHttpRequest) {
    return this.settings.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(new NotImplementedException());
        }
        const url = `${settings.authServerURL}${FRAPPE_API_GET_TOPIC_ENDPOINT}${FORWARD_SLASH}${addTopicPayload.topic_name}`;
        const headers = {
          headers: this.settings.getAuthorizationHeaders(
            clientHttpRequest.token,
          ),
        };
        return this.http.get(url, headers).pipe(
          map(res => res.data.data),
          switchMap(() => {
            return this.addCourseTopic(
              addTopicPayload,
              settings,
              clientHttpRequest,
            );
          }),
          catchError(err => {
            if (
              err.response &&
              err.response.data &&
              err.response.status === 404
            ) {
              return this.addNewTopic(
                addTopicPayload,
                settings,
                clientHttpRequest,
              );
            } else {
              return throwError(
                new BadRequestException(
                  this.settings.getErrorMessage(err.response.data),
                ),
              );
            }
          }),
        );
      }),
    );
  }

  addNewTopic(
    addTopicPayload: UpdateTopicStatusDto,
    settings: ServerSettings,
    clientHttpRequest,
  ) {
    const url = `${settings.authServerURL}${FRAPPE_API_GET_TOPIC_ENDPOINT}`;
    const headers = {
      headers: this.settings.getAuthorizationHeaders(clientHttpRequest.token),
    };
    const body = { topic_name: addTopicPayload.topic_name };
    return this.http.post(url, JSON.stringify(body), headers).pipe(
      map(res => res.data.data),
      switchMap(() => {
        return this.addCourseTopic(
          addTopicPayload,
          settings,
          clientHttpRequest,
        );
      }),
    );
  }

  addCourseTopic(
    addTopicPayload: UpdateTopicStatusDto,
    settings: ServerSettings,
    clientHttpRequest,
  ) {
    const url = `${settings.authServerURL}${FRAPPE_API_GET_COURSE_ENDPOINT}${addTopicPayload.course_name}`;
    const headers = {
      headers: this.settings.getAuthorizationHeaders(clientHttpRequest.token),
    };
    return this.http.get(url, headers).pipe(
      map(res => res.data),
      switchMap(getCourseData => {
        getCourseData.data.topics.push({
          topic: addTopicPayload.topic_name,
          topic_name: addTopicPayload.topic_name,
        });
        const body = {
          topics: getCourseData.data.topics,
        };
        return this.http.put(url, JSON.stringify(body), headers);
      }),
      map(res => res.data.data),
      switchMap(res => {
        return this.courseWebhookService.courseUpdated(res);
      }),
    );
  }
}
