import { Injectable, BadRequestException } from '@nestjs/common';
import { TopicWebhookDto } from '../../entity/topic/topic-webhook-dto';
import { TopicService } from '../../entity/topic/topic.service';
import { Topic } from '../../entity/topic/topic.entity';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { TOPIC_ALREADY_EXISTS } from '../../../constants/messages';
import * as uuidv4 from 'uuid/v4';
@Injectable()
export class TopicWebhookAggregateService {
  constructor(private readonly topicService: TopicService) {}

  topicCreated(topicPayload: TopicWebhookDto) {
    return from(
      this.topicService.findOne({
        name: topicPayload.name,
      }),
    ).pipe(
      switchMap(topic => {
        if (topic) {
          return throwError(new BadRequestException(TOPIC_ALREADY_EXISTS));
        }
        const provider = this.mapTopic(topicPayload);
        return from(this.topicService.create(provider)).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }

  mapTopic(topicPayload: TopicWebhookDto) {
    const topic = new Topic();
    Object.assign(topic, topicPayload);
    topic.isSynced = true;
    topic.uuid = uuidv4();
    return topic;
  }

  topicDeleted(topicPayload: TopicWebhookDto) {
    this.topicService.deleteOne({ name: topicPayload.name });
  }

  topicUpdated(topicPayload: TopicWebhookDto) {
    return from(this.topicService.findOne({ name: topicPayload.name })).pipe(
      switchMap(topic => {
        if (!topic) {
          return this.topicCreated(topicPayload);
        }
        topic.isSynced = true;
        return from(
          this.topicService.updateOne(
            { name: topic.name },
            { $set: topicPayload },
          ),
        ).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }
}
