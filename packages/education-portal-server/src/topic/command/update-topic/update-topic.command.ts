import { ICommand } from '@nestjs/cqrs';
import { UpdateTopicDto } from '../../entity/topic/update-topic-dto';

export class UpdateTopicCommand implements ICommand {
  constructor(public readonly updatePayload: UpdateTopicDto) {}
}
