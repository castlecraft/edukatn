import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveTopicCommand } from './remove-topic.command';
import { TopicAggregateService } from '../../aggregates/topic-aggregate/topic-aggregate.service';

@CommandHandler(RemoveTopicCommand)
export class RemoveTopicCommandHandler
  implements ICommandHandler<RemoveTopicCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: TopicAggregateService,
  ) {}
  async execute(command: RemoveTopicCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.remove(uuid);
    aggregate.commit();
  }
}
