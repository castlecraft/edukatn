import { IsNotEmpty, IsOptional, IsString, IsNumber } from 'class-validator';
export class UpdateAttendanceDto {
  @IsNotEmpty()
  uuid: string;

  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  student: string;

  @IsOptional()
  @IsString()
  course_schedule: string;

  @IsOptional()
  @IsString()
  date: string;

  @IsOptional()
  @IsString()
  student_name: string;

  @IsOptional()
  @IsString()
  student_group: string;

  @IsOptional()
  @IsString()
  status: string;

  @IsOptional()
  @IsString()
  doctype: string;
}
