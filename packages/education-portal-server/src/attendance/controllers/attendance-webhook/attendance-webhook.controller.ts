import {
  Controller,
  Post,
  Body,
  UsePipes,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { AttendanceWebhookDto } from '../../entity/attendance/attendance-webhook-dto';
import { AttendanceWebhookAggregateService } from '../../aggregates/attendance-webhook-aggregate/attendance-webhook-aggregate.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';

@Controller('attendance')
export class AttendanceWebhookController {
  constructor(
    private readonly attendanceWebhookAggreagte: AttendanceWebhookAggregateService,
  ) {}
  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  attendanceCreated(@Body() attendancePayload: AttendanceWebhookDto) {
    return this.attendanceWebhookAggreagte.attendanceCreated(attendancePayload);
  }

  @Post('webhook/v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  attendanceUpdated(@Body() attendancePayload: AttendanceWebhookDto) {
    return this.attendanceWebhookAggreagte.attendanceUpdated(attendancePayload);
  }

  @Post('webhook/v1/delete')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  attendanceDeleted(@Body() attendancePayload: AttendanceWebhookDto) {
    return this.attendanceWebhookAggreagte.attendanceDeleted(attendancePayload);
  }
}
