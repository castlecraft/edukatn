import { IQuery } from '@nestjs/cqrs';

export class RetrieveAttendanceQuery implements IQuery {
  constructor(public readonly uuid: string, public readonly req: any) {}
}
