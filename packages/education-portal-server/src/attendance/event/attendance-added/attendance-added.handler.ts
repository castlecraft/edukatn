import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AttendanceAddedEvent } from './attendance-added.event';
import { AttendanceService } from '../../entity/attendance/attendance.service';

@EventsHandler(AttendanceAddedEvent)
export class AttendanceAddedCommandHandler
  implements IEventHandler<AttendanceAddedEvent> {
  constructor(private readonly attendanceService: AttendanceService) {}
  async handle(event: AttendanceAddedEvent) {
    const { attendance } = event;
    await this.attendanceService.create(attendance);
  }
}
