import { IEvent } from '@nestjs/cqrs';
import { Attendance } from '../../entity/attendance/attendance.entity';

export class AttendanceAddedEvent implements IEvent {
  constructor(public attendance: Attendance, public clientHttpRequest: any) {}
}
