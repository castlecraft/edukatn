import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AttendanceService } from '../../entity/attendance/attendance.service';
import { AttendanceRemovedEvent } from './attendance-removed.event';

@EventsHandler(AttendanceRemovedEvent)
export class AttendanceRemovedCommandHandler
  implements IEventHandler<AttendanceRemovedEvent> {
  constructor(private readonly attendanceService: AttendanceService) {}
  async handle(event: AttendanceRemovedEvent) {
    const { attendance } = event;
    await this.attendanceService.deleteOne({ uuid: attendance.uuid });
  }
}
