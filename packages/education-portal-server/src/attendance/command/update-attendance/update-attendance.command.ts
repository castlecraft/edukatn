import { ICommand } from '@nestjs/cqrs';
import { UpdateAttendanceDto } from '../../entity/attendance/update-attendance-dto';

export class UpdateAttendanceCommand implements ICommand {
  constructor(public readonly updatePayload: UpdateAttendanceDto) {}
}
