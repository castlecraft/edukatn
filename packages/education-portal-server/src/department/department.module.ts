import { Module, HttpModule } from '@nestjs/common';
import { DepartmentAggregatesManager } from './aggregates';
import { DepartmentEntitiesModule } from './entity/entity.module';
import { DepartmentQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { DepartmentCommandManager } from './command';
import { DepartmentEventManager } from './event';
import { DepartmentController } from './controllers/department/department.controller';
import { DepartmentPoliciesService } from './policies/department-policies/department-policies.service';
import { DepartmentWebhookController } from './controllers/department-webhook/department-webhook.controller';

@Module({
  imports: [DepartmentEntitiesModule, CqrsModule, HttpModule],
  controllers: [DepartmentController, DepartmentWebhookController],
  providers: [
    ...DepartmentAggregatesManager,
    ...DepartmentQueryManager,
    ...DepartmentEventManager,
    ...DepartmentCommandManager,
    DepartmentPoliciesService,
  ],
  exports: [DepartmentEntitiesModule],
})
export class DepartmentModule {}
