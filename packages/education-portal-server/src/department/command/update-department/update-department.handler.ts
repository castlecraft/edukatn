import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateDepartmentCommand } from './update-department.command';
import { DepartmentAggregateService } from '../../aggregates/department-aggregate/department-aggregate.service';

@CommandHandler(UpdateDepartmentCommand)
export class UpdateDepartmentCommandHandler
  implements ICommandHandler<UpdateDepartmentCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: DepartmentAggregateService,
  ) {}

  async execute(command: UpdateDepartmentCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.update(updatePayload);
    aggregate.commit();
  }
}
