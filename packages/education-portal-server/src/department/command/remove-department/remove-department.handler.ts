import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveDepartmentCommand } from './remove-department.command';
import { DepartmentAggregateService } from '../../aggregates/department-aggregate/department-aggregate.service';

@CommandHandler(RemoveDepartmentCommand)
export class RemoveDepartmentCommandHandler
  implements ICommandHandler<RemoveDepartmentCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: DepartmentAggregateService,
  ) {}
  async execute(command: RemoveDepartmentCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.remove(uuid);
    aggregate.commit();
  }
}
