import { IEvent } from '@nestjs/cqrs';
import { Department } from '../../entity/department/department.entity';

export class DepartmentUpdatedEvent implements IEvent {
  constructor(public updatePayload: Department) {}
}
