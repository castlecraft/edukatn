import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { DepartmentDto } from '../../entity/department/department-dto';
import { Department } from '../../entity/department/department.entity';
import { DepartmentAddedEvent } from '../../event/department-added/department-added.event';
import { DepartmentService } from '../../entity/department/department.service';
import { DepartmentRemovedEvent } from '../../event/department-removed/department-removed.event';
import { DepartmentUpdatedEvent } from '../../event/department-updated/department-updated.event';
import { UpdateDepartmentDto } from '../../entity/department/update-department-dto';

@Injectable()
export class DepartmentAggregateService extends AggregateRoot {
  constructor(private readonly departmentService: DepartmentService) {
    super();
  }

  addDepartment(departmentPayload: DepartmentDto, clientHttpRequest) {
    const department = new Department();
    Object.assign(department, departmentPayload);
    department.uuid = uuidv4();
    this.apply(new DepartmentAddedEvent(department, clientHttpRequest));
  }

  async retrieveDepartment(uuid: string, req) {
    const provider = await this.departmentService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getDepartmentList(offset, limit, sort, search, clientHttpRequest) {
    return await this.departmentService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.departmentService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new DepartmentRemovedEvent(found));
  }

  async update(updatePayload: UpdateDepartmentDto) {
    const provider = await this.departmentService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new DepartmentUpdatedEvent(update));
  }
}
