import { Test, TestingModule } from '@nestjs/testing';
import { DepartmentAggregateService } from './department-aggregate.service';
import { DepartmentService } from '../../entity/department/department.service';

describe('DepartmentAggregateService', () => {
  let service: DepartmentAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DepartmentAggregateService,
        {
          provide: DepartmentService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<DepartmentAggregateService>(
      DepartmentAggregateService,
    );
  });
  DepartmentAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
