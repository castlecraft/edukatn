import { DepartmentAggregateService } from './department-aggregate/department-aggregate.service';
import { DepartmentWebhookAggregateService } from './department-webhook-aggregate/department-webhook-aggregate.service';

export const DepartmentAggregatesManager = [
  DepartmentAggregateService,
  DepartmentWebhookAggregateService,
];
