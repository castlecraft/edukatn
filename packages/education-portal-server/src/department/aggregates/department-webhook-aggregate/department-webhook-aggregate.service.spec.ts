import { Test, TestingModule } from '@nestjs/testing';
import { DepartmentWebhookAggregateService } from './department-webhook-aggregate.service';
import { DepartmentService } from '../../../department/entity/department/department.service';

describe('DepartmentWebhookAggregateService', () => {
  let service: DepartmentWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DepartmentWebhookAggregateService,
        {
          provide: DepartmentService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<DepartmentWebhookAggregateService>(
      DepartmentWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
