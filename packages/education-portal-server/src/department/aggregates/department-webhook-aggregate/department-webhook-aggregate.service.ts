import { Injectable, BadRequestException } from '@nestjs/common';
import { DepartmentWebhookDto } from '../../entity/department/department-webhook-dto';
import { DepartmentService } from '../../../department/entity/department/department.service';
import { Department } from '../../../department/entity/department/department.entity';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { DEPARTMENT_ALREADY_EXISTS } from '../../../constants/messages';
import * as uuidv4 from 'uuid/v4';
@Injectable()
export class DepartmentWebhookAggregateService {
  constructor(private readonly departmentService: DepartmentService) {}

  departmentCreated(departmentPayload: DepartmentWebhookDto) {
    return from(
      this.departmentService.findOne({
        name: departmentPayload.name,
      }),
    ).pipe(
      switchMap(department => {
        if (department) {
          return throwError(new BadRequestException(DEPARTMENT_ALREADY_EXISTS));
        }
        const provider = this.mapDepartment(departmentPayload);
        return from(this.departmentService.create(provider)).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }

  mapDepartment(departmentPayload: DepartmentWebhookDto) {
    const department = new Department();
    Object.assign(department, departmentPayload);
    department.isSynced = true;
    department.uuid = uuidv4();
    return department;
  }

  departmentDeleted(departmentPayload: DepartmentWebhookDto) {
    this.departmentService.deleteOne({ name: departmentPayload.name });
  }

  departmentUpdated(departmentPayload: DepartmentWebhookDto) {
    return from(
      this.departmentService.findOne({ name: departmentPayload.name }),
    ).pipe(
      switchMap(department => {
        if (!department) {
          return this.departmentCreated(departmentPayload);
        }
        department.isSynced = true;
        return from(
          this.departmentService.updateOne(
            { name: department.name },
            { $set: departmentPayload },
          ),
        ).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }
}
