export function studentOnUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Student',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'owner',
        key: 'owner',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'enabled',
        key: 'enabled',
      },
      {
        fieldname: 'first_name',
        key: 'first_name',
      },
      {
        fieldname: 'middle_name',
        key: 'middle_name',
      },
      {
        fieldname: 'last_name',
        key: 'last_name',
      },
      {
        fieldname: 'naming_series',
        key: 'naming_series',
      },
      {
        fieldname: 'student_email_id',
        key: 'student_email_id',
      },
      {
        fieldname: 'student_mobile_number',
        key: 'student_mobile_number',
      },
      {
        fieldname: 'joining_date',
        key: 'joining_date',
      },
      {
        fieldname: 'date_of_birth',
        key: 'date_of_birth',
      },
      {
        fieldname: 'blood_group',
        key: 'blood_group',
      },
      {
        fieldname: 'gender',
        key: 'gender',
      },
      {
        fieldname: 'nationality',
        key: 'nationality',
      },
      {
        fieldname: 'address_line_1',
        key: 'address_line_1',
      },
      {
        fieldname: 'title',
        key: 'title',
      },
      {
        fieldname: 'guardians',
        key: 'guardians',
      },
    ],
  };
}
export function studentAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Student',
    webhook_docevent: 'after_insert',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'owner',
        key: 'owner',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'enabled',
        key: 'enabled',
      },
      {
        fieldname: 'first_name',
        key: 'first_name',
      },
      {
        fieldname: 'middle_name',
        key: 'middle_name',
      },
      {
        fieldname: 'last_name',
        key: 'last_name',
      },
      {
        fieldname: 'naming_series',
        key: 'naming_series',
      },
      {
        fieldname: 'student_email_id',
        key: 'student_email_id',
      },
      {
        fieldname: 'student_mobile_number',
        key: 'student_mobile_number',
      },
      {
        fieldname: 'joining_date',
        key: 'joining_date',
      },
      {
        fieldname: 'date_of_birth',
        key: 'date_of_birth',
      },
      {
        fieldname: 'blood_group',
        key: 'blood_group',
      },
      {
        fieldname: 'gender',
        key: 'gender',
      },
      {
        fieldname: 'nationality',
        key: 'nationality',
      },
      {
        fieldname: 'address_line_1',
        key: 'address_line_1',
      },
      {
        fieldname: 'title',
        key: 'title',
      },
      {
        fieldname: 'guardians',
        key: 'guardians',
      },
    ],
  };
}
export function studentOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Student',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'owner',
        key: 'owner',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'enabled',
        key: 'enabled',
      },
      {
        fieldname: 'first_name',
        key: 'first_name',
      },
      {
        fieldname: 'middle_name',
        key: 'middle_name',
      },
      {
        fieldname: 'last_name',
        key: 'last_name',
      },
      {
        fieldname: 'naming_series',
        key: 'naming_series',
      },
      {
        fieldname: 'student_email_id',
        key: 'student_email_id',
      },
      {
        fieldname: 'student_mobile_number',
        key: 'student_mobile_number',
      },
      {
        fieldname: 'joining_date',
        key: 'joining_date',
      },
      {
        fieldname: 'date_of_birth',
        key: 'date_of_birth',
      },
      {
        fieldname: 'blood_group',
        key: 'blood_group',
      },
      {
        fieldname: 'gender',
        key: 'gender',
      },
      {
        fieldname: 'nationality',
        key: 'nationality',
      },
      {
        fieldname: 'address_line_1',
        key: 'address_line_1',
      },
      {
        fieldname: 'title',
        key: 'title',
      },
      {
        fieldname: 'guardians',
        key: 'guardians',
      },
    ],
  };
}
export function teacherOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Instructor',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'instructor_name',
        key: 'instructor_name',
      },
      {
        fieldname: 'employee',
        key: 'employee',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'instructor_log',
        key: 'instructor_log',
      },
    ],
  };
}
export function teacherOnUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Instructor',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'instructor_name',
        key: 'instructor_name',
      },
      {
        fieldname: 'employee',
        key: 'employee',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'instructor_log',
        key: 'instructor_log',
      },
    ],
  };
}
export function teacherAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Instructor',
    webhook_docevent: 'after_insert',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'instructor_name',
        key: 'instructor_name',
      },
      {
        fieldname: 'employee',
        key: 'employee',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'instructor_log',
        key: 'instructor_log',
      },
    ],
  };
}
export function programOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Program',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'program_name',
        key: 'program_name',
      },
      {
        fieldname: 'department',
        key: 'department',
      },
      {
        fieldname: 'is_published',
        key: 'is_published',
      },
      {
        fieldname: 'allow_self_enroll',
        key: 'allow_self_enroll',
      },
      {
        fieldname: 'is_featured',
        key: 'is_featured',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'courses',
        key: 'courses',
      },
    ],
  };
}
export function programAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Program',
    webhook_docevent: 'after_insert',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'program_name',
        key: 'program_name',
      },
      {
        fieldname: 'department',
        key: 'department',
      },
      {
        fieldname: 'is_published',
        key: 'is_published',
      },
      {
        fieldname: 'allow_self_enroll',
        key: 'allow_self_enroll',
      },
      {
        fieldname: 'is_featured',
        key: 'is_featured',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'courses',
        key: 'courses',
      },
    ],
  };
}
export function programOnUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Program',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'program_name',
        key: 'program_name',
      },
      {
        fieldname: 'department',
        key: 'department',
      },
      {
        fieldname: 'is_published',
        key: 'is_published',
      },
      {
        fieldname: 'allow_self_enroll',
        key: 'allow_self_enroll',
      },
      {
        fieldname: 'is_featured',
        key: 'is_featured',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'courses',
        key: 'courses',
      },
    ],
  };
}
export function courseOnUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Course',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'course_name',
        key: 'course_name',
      },
      {
        fieldname: 'department',
        key: 'department',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'topics',
        key: 'topics',
      },
    ],
  };
}
export function courseOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Course',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'course_name',
        key: 'course_name',
      },
      {
        fieldname: 'department',
        key: 'department',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'topics',
        key: 'topics',
      },
    ],
  };
}
export function courseAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Course',
    webhook_docevent: 'after_insert',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'course_name',
        key: 'course_name',
      },
      {
        fieldname: 'department',
        key: 'department',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'topics',
        key: 'topics',
      },
    ],
  };
}
export function topicOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Topic',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'topic_name',
        key: 'topic_name',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'topic_content',
        key: 'topic_content',
      },
    ],
  };
}
export function topicOnUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Topic',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'topic_name',
        key: 'topic_name',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'topic_content',
        key: 'topic_content',
      },
    ],
  };
}
export function topicAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Topic',
    webhook_docevent: 'after_insert',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'topic_name',
        key: 'topic_name',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'topic_content',
        key: 'topic_content',
      },
    ],
  };
}
export function videoOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Video',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'title',
        key: 'title',
      },
      {
        fieldname: 'provider',
        key: 'provider',
      },
      {
        fieldname: 'url',
        key: 'url',
      },
      {
        fieldname: 'publish_date',
        key: 'publish_date',
      },
      {
        fieldname: 'duration',
        key: 'duration',
      },
      {
        fieldname: 'description',
        key: 'description',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
    ],
  };
}
export function videoOnUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Video',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'title',
        key: 'title',
      },
      {
        fieldname: 'provider',
        key: 'provider',
      },
      {
        fieldname: 'url',
        key: 'url',
      },
      {
        fieldname: 'publish_date',
        key: 'publish_date',
      },
      {
        fieldname: 'duration',
        key: 'duration',
      },
      {
        fieldname: 'description',
        key: 'description',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
    ],
  };
}
export function videoAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Video',
    webhook_docevent: 'after_insert',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'title',
        key: 'title',
      },
      {
        fieldname: 'provider',
        key: 'provider',
      },
      {
        fieldname: 'url',
        key: 'url',
      },
      {
        fieldname: 'publish_date',
        key: 'publish_date',
      },
      {
        fieldname: 'duration',
        key: 'duration',
      },
      {
        fieldname: 'description',
        key: 'description',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
    ],
  };
}
export function articleOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Article',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'title',
        key: 'title',
      },
      {
        fieldname: 'author',
        key: 'author',
      },
      {
        fieldname: 'content',
        key: 'content',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
    ],
  };
}
export function articleAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Article',
    webhook_docevent: 'after_insert',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'title',
        key: 'title',
      },
      {
        fieldname: 'author',
        key: 'author',
      },
      {
        fieldname: 'content',
        key: 'content',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
    ],
  };
}
export function articleOnUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Article',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'title',
        key: 'title',
      },
      {
        fieldname: 'author',
        key: 'author',
      },
      {
        fieldname: 'content',
        key: 'content',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
    ],
  };
}
export function quizAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Quiz',
    webhook_docevent: 'after_insert',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'title',
        key: 'title',
      },
      {
        fieldname: 'passing_score',
        key: 'passing_score',
      },
      {
        fieldname: 'max_attempts',
        key: 'max_attempts',
      },
      {
        fieldname: 'grading_basis',
        key: 'grading_basis',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'question',
        key: 'question',
      },
    ],
  };
}
export function quizOnUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Quiz',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'title',
        key: 'title',
      },
      {
        fieldname: 'passing_score',
        key: 'passing_score',
      },
      {
        fieldname: 'max_attempts',
        key: 'max_attempts',
      },
      {
        fieldname: 'grading_basis',
        key: 'grading_basis',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'question',
        key: 'question',
      },
    ],
  };
}
export function quizOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Quiz',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'title',
        key: 'title',
      },
      {
        fieldname: 'passing_score',
        key: 'passing_score',
      },
      {
        fieldname: 'max_attempts',
        key: 'max_attempts',
      },
      {
        fieldname: 'grading_basis',
        key: 'grading_basis',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'question',
        key: 'question',
      },
    ],
  };
}
export function questionAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Question',
    webhook_docevent: 'after_insert',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'question',
        key: 'question',
      },
      {
        fieldname: 'question_type',
        key: 'question_type',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'options',
        key: 'options',
      },
    ],
  };
}
export function questionOnUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Question',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'question',
        key: 'question',
      },
      {
        fieldname: 'question_type',
        key: 'question_type',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'options',
        key: 'options',
      },
    ],
  };
}
export function questionOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Question',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'question',
        key: 'question',
      },
      {
        fieldname: 'question_type',
        key: 'question_type',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'options',
        key: 'options',
      },
    ],
  };
}
export function departmentAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Department',
    webhook_docevent: 'after_insert',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'department_name',
        key: 'department_name',
      },
      {
        fieldname: 'parent_department',
        key: 'parent_department',
      },
      {
        fieldname: 'company',
        key: 'company',
      },
      {
        fieldname: 'is_group',
        key: 'is_group',
      },
      {
        fieldname: 'disabled',
        key: 'disabled',
      },
      {
        fieldname: 'lft',
        key: 'lft',
      },
      {
        fieldname: 'rgt',
        key: 'rgt',
      },
      {
        fieldname: 'old_parent',
        key: 'old_parent',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'leave_approvers',
        key: 'leave_approvers',
      },
      {
        fieldname: 'expense_approvers',
        key: 'expense_approvers',
      },
    ],
  };
}
export function departmentOnUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Department',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'department_name',
        key: 'department_name',
      },
      {
        fieldname: 'parent_department',
        key: 'parent_department',
      },
      {
        fieldname: 'company',
        key: 'company',
      },
      {
        fieldname: 'is_group',
        key: 'is_group',
      },
      {
        fieldname: 'disabled',
        key: 'disabled',
      },
      {
        fieldname: 'lft',
        key: 'lft',
      },
      {
        fieldname: 'rgt',
        key: 'rgt',
      },
      {
        fieldname: 'old_parent',
        key: 'old_parent',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'leave_approvers',
        key: 'leave_approvers',
      },
      {
        fieldname: 'expense_approvers',
        key: 'expense_approvers',
      },
    ],
  };
}
export function departmentOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Department',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'department_name',
        key: 'department_name',
      },
      {
        fieldname: 'parent_department',
        key: 'parent_department',
      },
      {
        fieldname: 'company',
        key: 'company',
      },
      {
        fieldname: 'is_group',
        key: 'is_group',
      },
      {
        fieldname: 'disabled',
        key: 'disabled',
      },
      {
        fieldname: 'lft',
        key: 'lft',
      },
      {
        fieldname: 'rgt',
        key: 'rgt',
      },
      {
        fieldname: 'old_parent',
        key: 'old_parent',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'leave_approvers',
        key: 'leave_approvers',
      },
      {
        fieldname: 'expense_approvers',
        key: 'expense_approvers',
      },
    ],
  };
}
export function attendanceOnUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Student Attendance',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'student',
        key: 'student',
      },
      {
        fieldname: 'course_schedule',
        key: 'course_schedule',
      },
      {
        fieldname: 'date',
        key: 'date',
      },
      {
        fieldname: 'student_name',
        key: 'student_name',
      },
      {
        fieldname: 'student_group',
        key: 'student_group',
      },
      {
        fieldname: 'status',
        key: 'status',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
    ],
  };
}
export function attendanceOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Student Attendance',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'student',
        key: 'student',
      },
      {
        fieldname: 'course_schedule',
        key: 'course_schedule',
      },
      {
        fieldname: 'date',
        key: 'date',
      },
      {
        fieldname: 'student_name',
        key: 'student_name',
      },
      {
        fieldname: 'student_group',
        key: 'student_group',
      },
      {
        fieldname: 'status',
        key: 'status',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
    ],
  };
}
export function attendanceAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Student Attendance',
    webhook_docevent: 'after_insert',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'student',
        key: 'student',
      },
      {
        fieldname: 'course_schedule',
        key: 'course_schedule',
      },
      {
        fieldname: 'date',
        key: 'date',
      },
      {
        fieldname: 'student_name',
        key: 'student_name',
      },
      {
        fieldname: 'student_group',
        key: 'student_group',
      },
      {
        fieldname: 'status',
        key: 'status',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
    ],
  };
}
export function studentGroupAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Student Group',
    webhook_docevent: 'after_insert',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'academic_year',
        key: 'academic_year',
      },
      {
        fieldname: 'group_based_on',
        key: 'group_based_on',
      },
      {
        fieldname: 'student_group_name',
        key: 'student_group_name',
      },
      {
        fieldname: 'max_strength',
        key: 'max_strength',
      },
      {
        fieldname: 'academic_term',
        key: 'academic_term',
      },
      {
        fieldname: 'program',
        key: 'program',
      },
      {
        fieldname: 'course',
        key: 'course',
      },
      {
        fieldname: 'disabled',
        key: 'disabled',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'students',
        key: 'students',
      },
      {
        fieldname: 'instructors',
        key: 'instructors',
      },
    ],
  };
}
export function studentGroupOnUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Student Group',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'academic_year',
        key: 'academic_year',
      },
      {
        fieldname: 'group_based_on',
        key: 'group_based_on',
      },
      {
        fieldname: 'student_group_name',
        key: 'student_group_name',
      },
      {
        fieldname: 'max_strength',
        key: 'max_strength',
      },
      {
        fieldname: 'academic_term',
        key: 'academic_term',
      },
      {
        fieldname: 'program',
        key: 'program',
      },
      {
        fieldname: 'course',
        key: 'course',
      },
      {
        fieldname: 'disabled',
        key: 'disabled',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'students',
        key: 'students',
      },
      {
        fieldname: 'instructors',
        key: 'instructors',
      },
    ],
  };
}
export function studentGroupOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Student Group',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'academic_year',
        key: 'academic_year',
      },
      {
        fieldname: 'group_based_on',
        key: 'group_based_on',
      },
      {
        fieldname: 'student_group_name',
        key: 'student_group_name',
      },
      {
        fieldname: 'max_strength',
        key: 'max_strength',
      },
      {
        fieldname: 'academic_term',
        key: 'academic_term',
      },
      {
        fieldname: 'program',
        key: 'program',
      },
      {
        fieldname: 'course',
        key: 'course',
      },
      {
        fieldname: 'disabled',
        key: 'disabled',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
      {
        fieldname: 'students',
        key: 'students',
      },
      {
        fieldname: 'instructors',
        key: 'instructors',
      },
    ],
  };
}
export function roomOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Room',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'room_name',
        key: 'room_name',
      },
      {
        fieldname: 'room_number',
        key: 'room_number',
      },
      {
        fieldname: 'seating_capacity',
        key: 'seating_capacity',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
    ],
  };
}
export function roomAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Room',
    webhook_docevent: 'after_insert',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'room_name',
        key: 'room_name',
      },
      {
        fieldname: 'room_number',
        key: 'room_number',
      },
      {
        fieldname: 'seating_capacity',
        key: 'seating_capacity',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
    ],
  };
}
export function roomOnUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Room',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'room_name',
        key: 'room_name',
      },
      {
        fieldname: 'room_number',
        key: 'room_number',
      },
      {
        fieldname: 'seating_capacity',
        key: 'seating_capacity',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
    ],
  };
}
export function courseScheduleOnUpdateWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Course Schedule',
    webhook_docevent: 'on_update',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'student_group',
        key: 'student_group',
      },
      {
        fieldname: 'instructor',
        key: 'instructor',
      },
      {
        fieldname: 'instructor_name',
        key: 'instructor_name',
      },
      {
        fieldname: 'naming_series',
        key: 'naming_series',
      },
      {
        fieldname: 'course',
        key: 'course',
      },
      {
        fieldname: 'schedule_date',
        key: 'schedule_date',
      },
      {
        fieldname: 'room',
        key: 'room',
      },
      {
        fieldname: 'from_time',
        key: 'from_time',
      },
      {
        fieldname: 'to_time',
        key: 'to_time',
      },
      {
        fieldname: 'title',
        key: 'title',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
    ],
  };
}
export function courseScheduleAfterInsertWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Course Schedule',
    webhook_docevent: 'after_insert',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'student_group',
        key: 'student_group',
      },
      {
        fieldname: 'instructor',
        key: 'instructor',
      },
      {
        fieldname: 'instructor_name',
        key: 'instructor_name',
      },
      {
        fieldname: 'naming_series',
        key: 'naming_series',
      },
      {
        fieldname: 'course',
        key: 'course',
      },
      {
        fieldname: 'schedule_date',
        key: 'schedule_date',
      },
      {
        fieldname: 'room',
        key: 'room',
      },
      {
        fieldname: 'from_time',
        key: 'from_time',
      },
      {
        fieldname: 'to_time',
        key: 'to_time',
      },
      {
        fieldname: 'title',
        key: 'title',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
    ],
  };
}
export function courseScheduleOnTrashWebhookData(
  webhookURL: string,
  webhookApiKey: string,
) {
  return {
    webhook_doctype: 'Course Schedule',
    webhook_docevent: 'on_trash',
    request_url: webhookURL,
    request_structure: 'Form URL-Encoded',
    doctype: 'Webhook',
    webhook_headers: [
      {
        key: 'Content-Type',
        value: 'application/json',
      },
      {
        key: 'x-frappe-api-key',
        value: webhookApiKey,
      },
    ],
    webhook_data: [
      {
        fieldname: 'name',
        key: 'name',
      },
      {
        fieldname: 'docstatus',
        key: 'docstatus',
      },
      {
        fieldname: 'student_group',
        key: 'student_group',
      },
      {
        fieldname: 'instructor',
        key: 'instructor',
      },
      {
        fieldname: 'instructor_name',
        key: 'instructor_name',
      },
      {
        fieldname: 'naming_series',
        key: 'naming_series',
      },
      {
        fieldname: 'course',
        key: 'course',
      },
      {
        fieldname: 'schedule_date',
        key: 'schedule_date',
      },
      {
        fieldname: 'room',
        key: 'room',
      },
      {
        fieldname: 'from_time',
        key: 'from_time',
      },
      {
        fieldname: 'to_time',
        key: 'to_time',
      },
      {
        fieldname: 'title',
        key: 'title',
      },
      {
        fieldname: 'doctype',
        key: 'doctype',
      },
    ],
  };
}
