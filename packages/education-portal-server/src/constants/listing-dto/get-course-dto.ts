import { IsNotEmpty, IsString } from 'class-validator';

export class GetCourseQueryDto {
  @IsNotEmpty()
  @IsString()
  program_name: string;

  @IsNotEmpty()
  @IsString()
  course_name: string;
}
