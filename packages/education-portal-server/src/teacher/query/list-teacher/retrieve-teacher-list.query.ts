import { IQuery } from '@nestjs/cqrs';
import { ClientHttpRequestTokenInterface } from '../../../common/client-request-token.interace';

export class RetrieveTeacherListQuery implements IQuery {
  constructor(
    public offset: number,
    public limit: number,
    public search: string,
    public sort: string,
    public clientHttpRequest: ClientHttpRequestTokenInterface,
  ) {}
}
