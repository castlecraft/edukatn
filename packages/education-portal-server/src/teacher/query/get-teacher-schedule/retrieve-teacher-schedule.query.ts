import { IQuery } from '@nestjs/cqrs';
import { ClientHttpRequestTokenInterface } from '../../../common/client-request-token.interace';
import { ScheduleDateDto } from '../../../teacher/entity/teacher/schedule-date-dto';

export class RetrieveTeacherScheduleQuery implements IQuery {
  constructor(
    public readonly offset,
    public readonly limit,
    public readonly search,
    public readonly sort,
    public readonly datePayload: ScheduleDateDto,
    public readonly req: ClientHttpRequestTokenInterface,
  ) {}
}
