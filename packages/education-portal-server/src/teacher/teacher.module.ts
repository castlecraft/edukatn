import { Module, HttpModule } from '@nestjs/common';
import { TeacherAggregatesManager } from './aggregates';
import { TeacherEntitiesModule } from './entity/entity.module';
import { TeacherQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { TeacherCommandManager } from './command';
import { TeacherEventManager } from './event';
import { TeacherController } from './controllers/teacher/teacher.controller';
import { TeacherPoliciesService } from './policies/teacher-policies/teacher-policies.service';
import { TeacherWebhookAggregateService } from './aggregates/teacher-webhook-aggregate/teacher-webhook-aggregate.service';
import { TeacherWebhookController } from './controllers/teacher-webhook/teacher-webhook.controller';
import { RoomModule } from '../room/room.module';

@Module({
  imports: [RoomModule, TeacherEntitiesModule, CqrsModule, HttpModule],
  controllers: [TeacherController, TeacherWebhookController],
  providers: [
    ...TeacherAggregatesManager,
    ...TeacherQueryManager,
    ...TeacherEventManager,
    ...TeacherCommandManager,
    TeacherPoliciesService,
    TeacherWebhookAggregateService,
  ],
  exports: [TeacherEntitiesModule],
})
export class TeacherModule {}
