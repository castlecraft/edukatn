import { AddTeacherCommandHandler } from './add-teacher/add-teacher.handler';
import { RemoveTeacherCommandHandler } from './remove-teacher/remove-teacher.handler';
import { UpdateTeacherCommandHandler } from './update-teacher/update-teacher.handler';

export const TeacherCommandManager = [
  AddTeacherCommandHandler,
  RemoveTeacherCommandHandler,
  UpdateTeacherCommandHandler,
];
