import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Teacher } from './teacher/teacher.entity';
import { TeacherService } from './teacher/teacher.service';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [TypeOrmModule.forFeature([Teacher]), CqrsModule],
  providers: [TeacherService],
  exports: [TeacherService],
})
export class TeacherEntitiesModule {}
