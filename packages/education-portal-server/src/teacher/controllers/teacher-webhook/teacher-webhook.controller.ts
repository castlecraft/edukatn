import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  ValidationPipe,
  Body,
} from '@nestjs/common';
import { TeacherWebhookAggregateService } from '../../../teacher/aggregates/teacher-webhook-aggregate/teacher-webhook-aggregate.service';
import { TeacherWebhookDto } from '../../../teacher/entity/teacher/teacher-webhook-dto';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';

@Controller('teacher')
export class TeacherWebhookController {
  constructor(
    private readonly teacherAggregateService: TeacherWebhookAggregateService,
  ) {}

  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  createdTeacher(@Body() teacherPayload: TeacherWebhookDto) {
    return this.teacherAggregateService.createdTeacher(teacherPayload);
  }
  @Post('webhook/v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  updatedTeacher(@Body() teacherPayload: TeacherWebhookDto) {
    return this.teacherAggregateService.updatedTeacher(teacherPayload);
  }
  @Post('webhook/v1/delete')
  @UseGuards(FrappeWebhookGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  deletedTeacher(@Body() teacherPayload: TeacherWebhookDto) {
    return this.teacherAggregateService.deletedTeacher(teacherPayload);
  }
}
