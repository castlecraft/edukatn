import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TeacherDto } from '../../entity/teacher/teacher-dto';
import { AddTeacherCommand } from '../../command/add-teacher/add-teacher.command';
import { RemoveTeacherCommand } from '../../command/remove-teacher/remove-teacher.command';
import { UpdateTeacherCommand } from '../../command/update-teacher/update-teacher.command';
import { RetrieveTeacherQuery } from '../../query/get-teacher/retrieve-teacher.query';
import { RetrieveTeacherListQuery } from '../../query/list-teacher/retrieve-teacher-list.query';
import { UpdateTeacherDto } from '../../entity/teacher/update-teacher-dto';
import { ClientHttpRequestTokenInterface } from '../../../common/client-request-token.interace';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { RetrieveTeacherScheduleQuery } from '../../../teacher/query/get-teacher-schedule/retrieve-teacher-schedule.query';
import { ScheduleDateDto } from '../../../teacher/entity/teacher/schedule-date-dto';

@Controller('teacher')
export class TeacherController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(
    @Body() teacherPayload: TeacherDto,
    @Req() req: ClientHttpRequestTokenInterface,
  ) {
    return this.commandBus.execute(new AddTeacherCommand(teacherPayload, req));
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid: string) {
    return this.commandBus.execute(new RemoveTeacherCommand(uuid));
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getClient(
    @Param('uuid') uuid: string,
    @Req() req: ClientHttpRequestTokenInterface,
  ) {
    return await this.queryBus.execute(new RetrieveTeacherQuery(uuid, req));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getClientList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Req() clientHttpRequest: ClientHttpRequestTokenInterface,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveTeacherListQuery(
        offset,
        limit,
        sort,
        search,
        clientHttpRequest,
      ),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateClient(@Body() updatePayload: UpdateTeacherDto) {
    return this.commandBus.execute(new UpdateTeacherCommand(updatePayload));
  }

  @Get('v1/schedule')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  getTeacherSchedule(
    @Query('offset') limit = 10,
    @Query('limit') offset = 0,
    @Query('search') search = '',
    @Query('sort') sort,
    @Query() datePayload: ScheduleDateDto,
    @Req() req,
  ) {
    return this.queryBus.execute(
      new RetrieveTeacherScheduleQuery(
        limit,
        offset,
        search,
        sort,
        datePayload,
        req,
      ),
    );
  }
}
