import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FilePage } from './file.page';
import { CourseService } from '../course/course.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BehaviorSubject, of } from 'rxjs';

describe('FilePage', () => {
  let component: FilePage;
  let fixture: ComponentFixture<FilePage>;
  const selectedCourse = new BehaviorSubject<string>('');

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FilePage],
      imports: [IonicModule.forRoot(), HttpClientTestingModule],
      providers: [
        {
          provide: CourseService,
          useValue: {
            selectedCourse,
            getCourseList: (...args) => of([{}]),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(FilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
