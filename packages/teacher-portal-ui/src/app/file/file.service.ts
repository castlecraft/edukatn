import { Injectable } from '@angular/core';
import { Course } from '../common/interfaces/course.interface';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FileService {
  notesList: Array<Course>;

  constructor() {
    this.notesList = [
      {
        program: 'BSc it',
        course_name: 'programming in  c and c',
        notes: [
          {
            date: '21/3/2019',
            title: 'Here are all formulas of Motion',
            filename: [
              {
                fileName: 'gravity-documents_part_1.pdf',
              },
              {
                fileName: 'gravity-documents_part_1.pdf',
              },
            ],
          },
          {
            date: '21/3/2019',
            title: 'Explanation of Kinetic Energy.',
            filename: [
              {
                fileName: 'gravity-documents_part_1.pdf',
              },
              {
                fileName: 'gravity-documents_part_1.pdf',
              },
            ],
          },
        ],
      },
      {
        program: 'std xth',
        course_name: 'physics',
        notes: [
          {
            date: '12/12/2019',
            title: 'In this link their is brief explain Laws of Gravity',
            filename: [
              {
                fileName: 'gravity-documents_part_1.pdf',
              },
              {
                fileName: 'gravity-documents_part_1.pdf',
              },
            ],
          },
          {
            date: '13/01/2020',
            title:
              'This Link Contains all the Main Points of Chapter: Moving Charges and Magnetism',
            filename: [
              {
                fileName: 'gravity-documents_part_1.pdf',
              },
              {
                fileName: 'gravity-documents_part_1.pdf',
              },
            ],
          },
        ],
      },
      {
        program: 'BSc it',
        course_name: 'IONIC',
        notes: [
          {
            date: '21/2/2020',
            title: 'ion card',
            filename: [
              {
                fileName: 'gravity-documents_part_1.pdf',
              },
              {
                fileName: 'gravity-documents_part_1.pdf',
              },
            ],
          },
          {
            date: '28/4/2020',
            title: ' ion-list',
            filename: [
              {
                fileName: 'gravity-documents_part_1.pdf',
              },
              {
                fileName: 'gravity-documents_part_1.pdf',
              },
            ],
          },
        ],
      },
    ];
  }

  getNote(program: string, course_name: string) {
    let found: Course;
    found = {} as Course;

    for (const note of this.notesList) {
      if (note.program === program && note.course_name === course_name) {
        found = note;
      }
    }

    return of(found);
  }
}
