import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AttendancePage } from './attendance.page';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AttendanceService } from './attendance.service';
import { of, BehaviorSubject } from 'rxjs';
import { CourseService } from '../course/course.service';

describe('AttendancePage', () => {
  let component: AttendancePage;
  let fixture: ComponentFixture<AttendancePage>;
  const selectedCourse = new BehaviorSubject<string>('');
  const courseList = [];
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AttendancePage],
      imports: [IonicModule.forRoot()],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {
          provide: AttendanceService,
          useValue: {
            getAttendance: (...args) => of({ attendancesList: [] }),
          },
        },
        {
          provide: CourseService,
          useValue: {
            selectedCourse,
            courseList,
            getCourseList: (...args) => of([{}]),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AttendancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
