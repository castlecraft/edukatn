export interface Question {
  question: string;
  options: Array<Options>;
}
export interface Options {
  option: string;
  isCorrect?: boolean;
}
