import { Component, OnInit } from '@angular/core';
import { CourseService } from '../course/course.service';
import { Topic } from '../common/interfaces/topic.interface';
import { Course } from '../common/interfaces/course.interface';
import { QuizService } from './quiz.service';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.page.html',
  styleUrls: ['./quiz.page.scss'],
})
export class QuizPage implements OnInit {
  courseList: Array<Course>;
  selectedCourse: any;
  selectedTopic: Topic;
  topics: Array<Topic>;
  questions: Array<Topic>;
  is_correct: number;

  constructor(
    private courseService: CourseService,
    private quizService: QuizService,
  ) {
    this.courseList = [];
    this.selectedCourse = { program: '', course_name: '' } as any;
    this.selectedTopic = {} as Topic;
    this.questions = [];
    this.is_correct = 1;
  }

  ngOnInit() {
    if (this.courseService.selectedCourse.value.program !== '') {
      this.selectedCourse.program = this.courseService.selectedCourse.value.program;
      this.selectedCourse.course_name = this.courseService.selectedCourse.value.course_name;
      this.getCourse();
    }
    this.getCourseList();
  }
  compareObjects(o1: any, o2: any): boolean {
    return o1.program == o2.program && o1.course_name == o2.course_name;
  }

  getCourseList() {
    this.courseService.getCourseList().subscribe({
      next: res => {
        this.courseList = res;
        if (this.courseService.selectedCourse.value.program === '') {
          this.selectedCourse =
            this.courseList && this.courseList.length
              ? this.courseList[0]
              : { program: '', course_name: '' };
          this.getCourse();
        }
      },
    });
  }

  getCourse() {
    this.courseService.selectedCourse.next({
      program: this.selectedCourse.program,
      course_name: this.selectedCourse.course_name,
    });

    this.courseService
      .getCourse(this.selectedCourse.program, this.selectedCourse.course_name)
      .subscribe({
        next: res => {
          this.topics = res[0].topics;
        },
      });
  }

  getQueList() {
    this.questions = [];
    this.quizService
      .getQuiz(
        this.selectedCourse.program,
        this.selectedCourse.course_name,
        this.selectedTopic.topic_name,
      )
      .subscribe({
        next: res => {
          this.questions = res.docs;
        },
      });
  }
}
